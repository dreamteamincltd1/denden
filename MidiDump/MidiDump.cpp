/*
MidiDump
Reads MIDI file, writes content as text

Usage:
MidiDump.exe "input.mid" "output.txt" verbose_level
"input.mid" = path to MIDI file
"output.txt" = path to output dump file
	if empty - print output to screen
verbose_level = number of details:
	0 = Only header
	1 = Header & tracks
	2 = Header, tracks & events
	3 = Header, tracks & full events info
	default = 1
*/

#include <cstdio>
#include <cstdlib>
#include "../midi/include/midi.h"
#include "../midi/include/rw_error.h"


int main(int argc, char* argv[]) {
	if (argc < 2) {
		fprintf(stderr, "Usage:\nMidiDump.exe \"input.mid\" [\"output.txt\"] [verbose_level]\n");
		exit(-1);
	}
	char* file_in_name = argv[1];
	FILE* midi_file = fopen(file_in_name, "rb");
	if (!midi_file) {
		fprintf(stderr, "Unable to open MIDI file\n");
		exit(-2);
	}
	fseek(midi_file, 0, SEEK_END);
	size_t file_size = ftell(midi_file);
	rewind(midi_file);
	uint8_t* buf = new uint8_t[file_size];
	fread(buf, 1, file_size, midi_file);
	fclose(midi_file);

	midi::Midi midi_data;
	try {
		midi_data.read(buf);
	} catch (rw_error e) {
		fprintf(stderr, "MIDI file reading error: %s at %u\n", e.what(), e.where()-buf);
		exit(-3);
	} catch (std::exception e) {
		fprintf(stderr, "MIDI file error: %s\n", e.what());
		exit(-3);
	}
	delete[] buf;

	FILE* dump = stdout;
	if (argc >= 3 && *argv[2]) {
		dump = fopen(argv[2], "wt");
		if (!dump) {
			fprintf(stderr, "Unable to open dump file\n");
			exit(-2);
		}
	}
	int verbose_level = 1;
	if (argc >= 4) {
		verbose_level = strtol(argv[3], nullptr, 10);
	}
	midi_data.print_info(dump, verbose_level);
	if (dump != stdout) {
		fclose(dump);
	}
	return 0;
}