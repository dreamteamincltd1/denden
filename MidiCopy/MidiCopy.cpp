/*
MidiCopy
Reads MIDI file, writes data into another

Usage:
MidiDump.exe "input.mid" "output.mid"
"input.mid" = path to input MIDI file
"output.mid" = path to output MIDI file

*/

#include <cstdio>
#include <cstdlib>
#include "../midi/include/midi.h"
#include "../midi/include/integers.h"
#include "../midi/include/rw_error.h"

int main(int argc, char* argv[]) {
	if (argc < 3) {
		fprintf(stderr, "Usage:\nMidiCopy.exe \"input.mid\" \"output.mid\"\n");
		exit(-1);
	}
	FILE* midi_file = fopen(argv[1], "rb");
	if (!midi_file) {
		fprintf(stderr, "Unable to open input MIDI file\n");
		exit(-2);
	}
	fseek(midi_file, 0, SEEK_END);
	size_t file_size = ftell(midi_file);
	rewind(midi_file);
	uint8_t* buf = new uint8_t[file_size];
	fread(buf, 1, file_size, midi_file);
	fclose(midi_file);

	midi::Midi midi_data;
	try {
		midi_data.read(buf);
	} catch (rw_error e) {
		fprintf(stderr, "MIDI file reading error: %s at %u\n", e.what(), e.where()-buf);
		exit(-3);
	} catch (std::exception e) {
		fprintf(stderr, "MIDI file error: %s\n", e.what());
		exit(-3);
	}
	delete[] buf;

	FILE* midi_out = fopen(argv[2], "wb");
	if (!midi_out) {
		fprintf(stderr, "Unable to open output MIDI file\n");
		exit(-2);
	}

	// 4 MB should be enough for testing
	buf = new uint8_t[0x4000000];
	try {
		file_size = midi_data.write(buf);
	} catch (...) {
		fprintf(stderr, "Oops! Seems like you have a very big file=)\n");
		exit(-3);
	}

	fwrite(buf, 1, file_size, midi_out);
	fclose(midi_out);
	delete[] buf;
	return 0;
}