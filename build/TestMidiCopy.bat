@echo off
MidiCopy.exe "%~1" "%~2"
set verbose_level=3
if %ERRORLEVEL% EQU 0 (
	MidiDump.exe "%~1" "%~n1_original.txt" %verbose_level%
	MidiDump.exe "%~2" "%~n2_copied.txt" %verbose_level%
	fc /a "%~n1_original.txt" "%~n2_copied.txt"
	if %ERRORLEVEL% EQU 0 del "%~n1_original.txt" "%~n2_copied.txt"
) else (
	echo Unable to open file %1
)