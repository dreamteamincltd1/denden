#include "../include/rw_error.h"

rw_error::rw_error(const std::string& what, uint8_t* where) : invalid_argument(what), _where(where) {}
rw_error::rw_error(const char* what, uint8_t* where) : invalid_argument(what), _where(where) {}

uint8_t* rw_error::where() const { return _where; }