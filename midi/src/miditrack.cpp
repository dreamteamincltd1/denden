#include "../include/integers.h"
#include "../include/miditrack.h"
#include "../include/rw_error.h"
#include <memory>

namespace midi {
	const char MidiTrack::_SIGNATURE[4] = { 'M', 'T', 'r', 'k' };

	MidiTrack::MidiTrack() : _length(0), events(0) {}
	MidiTrack::MidiTrack(size_t evts) : _length(0), events(evts) {}

	void MidiTrack::size(const size_t len) {
		_length = len;
	}

	size_t MidiTrack::size() const {
		return _length;
	}

	size_t MidiTrack::read(uint8_t* buf) {
		uint8_t* start = buf;
		if (memcmp(buf, _SIGNATURE, 4)) throw rw_error(
			"Invalid track signature", buf
			);
		buf += 4;
		buf += read_uint32_be(_length, buf);

		uint8_t* ev = buf;
		while (true) {
			MidiEvent me;
			ev += me.read(ev);
			events.push_back(me);
			if (me.type() == MidiEvent::META_END_TRACK) break;
		}

		buf += _length;

		return buf - start;
	}

	size_t MidiTrack::write(uint8_t* buf) {
		uint8_t* start = buf;
		memcpy(buf, _SIGNATURE, 4);
		buf += 4;
		uint8_t* len_ptr = buf;
		buf += 4;
		uint8_t* events_start = buf;
		for (EventIterator evt = events.begin(); evt != events.end(); ++evt) {
			buf += evt->write(buf);
		}
		write_uint32_be(buf - events_start, len_ptr);
		return buf - start;
	}

}