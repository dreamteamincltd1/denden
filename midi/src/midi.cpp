#include "../include/midi.h"
#include "../include/midiheader.h"
#include "../include/miditrack.h"

#include <algorithm>
#include <deque>

#define PRINTF_NAME "%20s: "
#define TAB "    "
#define ENDL "\n"

namespace midi {
	size_t Midi::read(uint8_t* buf) {
		uint8_t* start = buf;
		buf += _header.read(buf);
		tracks.resize(_header.tracks_count());
		for (TrackIterator t = tracks.begin(); t != tracks.end(); ++t) {
			buf += t->read(buf);
		}
		return buf - start;
	}

	size_t Midi::write(uint8_t* buf) {
		uint8_t* start = buf;
		buf += _header.write(buf);
		for (TrackIterator t = tracks.begin(); t != tracks.end(); ++t) {
			buf += t->write(buf);
		}
		return buf - start;
	}

	void Midi::print_info(FILE* fp, unsigned verbose_level) {
		fprintf(fp, "MIDI {" ENDL);

		fprintf(fp, "MThd {" ENDL);
		fprintf(fp, TAB "File format = %u" ENDL, _header.format());
		fprintf(fp, TAB "Number of tracks = %u" ENDL, _header.tracks_count());
		fprintf(fp, TAB "Ticks per quarter note = %u" ENDL, _header.ticks_per_quarter_note());
		fprintf(fp, "}" ENDL);

		if (verbose_level >= 1) {
			for (size_t mtrk = 0; mtrk < tracks.size(); ++mtrk) {
				MidiTrack& track = tracks[mtrk];
				fprintf(fp, "MTrk %u {" ENDL, mtrk);
				fprintf(fp, TAB "Size = %u" ENDL, track.size());
				fprintf(fp, TAB "Events = %u" ENDL, track.events.size());
				if (verbose_level >= 2) {
					for (size_t mevt = 0; mevt < track.events.size(); ++mevt) {
						MidiEvent& event = track.events[mevt];
						fprintf(fp, TAB "%+8i %s", event.offset(),
							(event.is_meta() ? "META" : event.is_sysex() ? "SYSEX" : "MESSAGE"));
						if (verbose_level >= 3) {
							fprintf(fp, " {" ENDL);
							fprintf(fp, TAB TAB "Event = %s" ENDL, event.event_name());
							if (event.is_text()) {
								fprintf(fp, TAB TAB "Value = \"%.*s\"" ENDL, event.size(), event.data());
							} else if ((event.type() == MidiEvent::META_CH_PREFIX)
								|| (event.type() == MidiEvent::META_MIDI_PORT)) {
								fprintf(fp, TAB TAB "Value = %u" ENDL, event.value());
							} else if (event.type() == MidiEvent::META_SET_TEMPO) {
								fprintf(fp, TAB TAB "Value = %u" ENDL, event.value());
								fprintf(fp, TAB TAB "BPM = %u" ENDL, tempo2bpm(event.value()));
							} else if (event.type() == MidiEvent::META_TIME_SIGN) {
								fprintf(fp, TAB TAB "Time signature = %u/%u" ENDL, (unsigned)event.data(0), (1 << event.data(1)));
								fprintf(fp, TAB TAB "Clocks per metronome click = %u" ENDL, (unsigned)event.data(2));
								fprintf(fp, TAB TAB "32nd notes per MIDI quarter note = %u" ENDL, (unsigned)event.data(3));
							} else if (event.type() == MidiEvent::META_KEY_SIGN) {
								fprintf(fp, TAB TAB "Key signature = %i" ENDL, (int)(int8_t)event.data(0));
								fprintf(fp, TAB TAB "Tonality = %s" ENDL, (event.data(1) ? "minor" : "major"));
							} else if (event.is_meta() || event.is_sysex()) {
								if (event.size()) {
									fprintf(fp, TAB TAB "Value = ");
									for (size_t d = 0; d < event.size(); ++d) {
										fprintf(fp, "%02X", (unsigned)event.data(d));
									}
									fprintf(fp, ENDL);
								}
							} else {
								fprintf(fp, TAB TAB "Channel = %i" ENDL, event.channel());
								if (event.type() == MidiEvent::MSG_PROGRAM) {
									fprintf(fp, TAB TAB "Instrument = %s / %s" ENDL, event.instrument_family(), event.instrument_name());
								} else {
									if (event.is_key()) {
										if (event.channel() != MidiEvent::PERCUSSION_CHANNEL) {
											fprintf(fp, TAB TAB "Note = %s %i" ENDL, event.note_name(), event.octave());
										} else {
											fprintf(fp, TAB TAB "Percussion = %s" ENDL, event.note_name());
										}
									} else if (event.type() == MidiEvent::MSG_CONTROLLER) {
										fprintf(fp, TAB TAB "Controller = %s" ENDL, event.controller_name());
									}
									fprintf(fp, TAB TAB "Value = %u" ENDL, event.value());
								}
							}
							fprintf(fp, TAB "}");
						}
						fprintf(fp, ENDL);
					}
				}
				fprintf(fp, "}" ENDL);
			}
		}

		fprintf(fp, "}" ENDL);

	}

	size_t Midi::ticks_per_quarter_note() const {
		return _header.ticks_per_quarter_note();
	}

	void Midi::ticks_per_quarter_note(size_t ticks) {
		_header.ticks_per_quarter_note(ticks);
	}


	struct MidiEventAbsTime {
		MidiEvent* me;
		size_t track_id;
		size_t abs_time;
	};
	
	struct compareMEAT {
		bool operator() (const MidiEventAbsTime& meat1, const MidiEventAbsTime& meat2) const {
			return meat1.abs_time < meat2.abs_time;
		}
	};

	bool cmpMEAT(const MidiEventAbsTime& meat1, const MidiEventAbsTime& meat2) {
		return (meat1.abs_time < meat2.abs_time) 
			|| ((meat1.abs_time == meat2.abs_time) && (meat1.track_id < meat2.track_id));
	}

	void Midi::setHeader(const MidiHeader& header){
		_header = header;
	}

	Midi Midi::merge() const {
		size_t tracks_count = tracks.size();
		if (tracks_count == 1) return *this;
		std::deque<MidiEventAbsTime> srt_evts;
		std::deque<size_t> end_tracks(tracks_count, 0);
		for (size_t t = 0; t < tracks_count; ++t) {
			size_t abs_time = 0;
			size_t events_count = tracks[t].events.size();
			for (size_t e = 0; e < events_count; ++e) {
				MidiEventAbsTime meat;
				meat.track_id = t;
				meat.me = (MidiEvent*)(void*)&tracks[t].events[e];
				abs_time += meat.me->offset();
				if (meat.me->type() == MidiEvent::META_END_TRACK) {
					end_tracks[t] = abs_time;
					break;
				}
				meat.abs_time = abs_time;
				srt_evts.push_back(meat);
			}
		}

		std::stable_sort(srt_evts.begin(), srt_evts.end(), cmpMEAT);

		MidiEvent evt_end = MidiEvent::EndOfTrack();
		MidiEventAbsTime end_meat;
		end_meat.me = &evt_end;
		end_meat.track_id = 0;
		end_meat.abs_time = *std::max_element(end_tracks.begin(), end_tracks.end());
		srt_evts.push_back(end_meat);

		// tmp buffer for counting track length
		uint8_t* buf = new uint8_t[4096];

		size_t track_len = 0;
		size_t abs_time = 0;
		size_t total_evts = srt_evts.size();
		MidiTrack merged_track(total_evts);
		for (size_t e = 0; e < total_evts; ++e) {
			MidiEventAbsTime& meat = srt_evts[e];
			MidiEvent me = MidiEvent(*meat.me);
			me.offset(meat.abs_time - abs_time);
			merged_track.events[e] = me;
			abs_time = meat.abs_time;
			track_len += me.write(buf);
		}
		merged_track.size(track_len);
		delete[] buf;

		Midi merged_midi;
		merged_midi._header = MidiHeader::SingleTrack(_header.ticks_per_quarter_note());
		merged_midi.tracks.push_back(merged_track);
		return merged_midi;
	}
}