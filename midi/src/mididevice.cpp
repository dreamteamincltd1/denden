//#pragma comment(lib, "Winmm.lib")
#include "../include/mididevice.h"
#include <stdexcept>
#include <cstdint>

namespace midi {

	void MidiDevice::sleep_tacts(size_t t) {
		LARGE_INTEGER time;
		long long wait, scale;
		if (t <= 0) return;
		if (!_tick.QuadPart) QueryPerformanceCounter(&_tick);

		wait = _cpu_freq.QuadPart * _tempo * t;
		scale = _ticks * 1000000ll;

		do {
			QueryPerformanceCounter(&time);
		} while ((time.QuadPart - _tick.QuadPart) * scale < wait);
		_tick.QuadPart = time.QuadPart;
	}

	MidiDevice::MidiDevice(bool precise) {
		MMRESULT err = midiOutOpen(&_out, 0, 0, 0, CALLBACK_NULL);
		if (err != MMSYSERR_NOERROR) throw std::runtime_error(
			"Unable to open default MIDI device"
			);
		_tempo = 500000;
		_ticks = 96;
		_precise = precise;
		_playing = false;
		_tick.QuadPart = 0;
		QueryPerformanceFrequency(&_cpu_freq);
	}

	MidiDevice::~MidiDevice() {
		midiOutClose(_out);
	}

	void MidiDevice::play(const MidiEvent& me, bool delayed) {
		if (delayed) {
			if (_precise) {
				sleep_tacts(me.offset());
			} else {
				Sleep((_tempo * me.offset()) / (_ticks * 1000));
			}
		}
		if (me.is_meta()) {
			if (me.type() == MidiEvent::META_SET_TEMPO) {
				tempo(me.value());
			}
		} else if (me.is_sysex()) {
			// 
		} else {
			midiOutShortMsg(_out, me.getShortMessage());
		}
	}

	void MidiDevice::stop() {
		_playing = false;
		_tick.QuadPart = 0;
		midiOutReset(_out);
	}

	size_t MidiDevice::tempo() const {
		return _tempo;
	}

	void MidiDevice::tempo(size_t tempo) {
		if (tempo & 0xff000000) throw std::invalid_argument(
			"Invalid tempo size"
			);
		_tempo = tempo;
	}

	size_t MidiDevice::ticks_per_quarter_note() const {
		return _ticks;
	}

	void MidiDevice::ticks_per_quarter_note(size_t ticks) {
		if (ticks & 0xffff8000) throw std::invalid_argument(
			"Invalid ticks size"
			);
		_ticks = ticks;
	}
}