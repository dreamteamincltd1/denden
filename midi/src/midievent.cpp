#include "../include/integers.h"
#include "../include/midievent.h"
#include "../include/rw_error.h"

#include <memory>
#include <stdexcept>

namespace midi {
	MidiEvent::MidiEvent() :_data(nullptr), _size(0), _channel(0), _offset(0), _type(0) {}

	MidiEvent::MidiEvent(size_t size) :
		_size(size),
		_channel(0),
		_offset(0),
		_type(0) {
		if (_size) {
			_data = new uint8_t[_size];
		} else {
			_data = nullptr;
		}
	}

	MidiEvent::MidiEvent(const MidiEvent& me) :
		_size(me._size),
		_channel(me._channel),
		_offset(me._offset),
		_type(me._type) {
		if (_size) {
			_data = new uint8_t[_size];
			memcpy(_data, me._data, _size);
		} else {
			_data = nullptr;
		}
	}

	void MidiEvent::operator=(const MidiEvent& me) {
		if (me._data && (_data == me._data)) return;
		clear();
		_size = me._size;
		_channel = me._channel;
		_offset = me._offset;
		_type = me._type;
		if (_size) {
			_data = new uint8_t[_size];
			memcpy(_data, me._data, _size);
		} else {
			_data = nullptr;
		}

	}

	MidiEvent::~MidiEvent() {
		clear();
	}

	MidiEvent& MidiEvent::offset(size_t offset_) {
		if (offset_ & 0xf0000000) throw std::out_of_range(
			"Offset is too large"
			);
		_offset = offset_;
		return *this;
	}

	MidiEvent& MidiEvent::channel(uint8_t channel_) {
		if (channel_ > 0x0f) throw std::out_of_range(
			"Wrong channel number"
			);
		if (is_meta() || is_sysex()) throw std::invalid_argument(
			"This event doesnt support channels"
			);
		_channel = channel_;
		return *this;
	}

	MidiEvent& MidiEvent::data(size_t index, uint8_t value) {
		if (index >= _size) throw std::out_of_range(
			"Attempt to write to out of range"
			);
		_data[index] = value;
		return *this;
	}

	bool MidiEvent::is_meta() const {
		return (_type & 0xff00) == 0xff00;
	}

	bool MidiEvent::is_sysex() const {
		return (_type & 0xfff0) == 0x00f0;
	}

	bool MidiEvent::is_text() const {
		return (_type == MidiEvent::META_TEXT)
			|| (_type == MidiEvent::META_COPYRIGHT)
			|| (_type == MidiEvent::META_TRACK_NAME)
			|| (_type == MidiEvent::META_INSTRUMENT)
			|| (_type == MidiEvent::META_LYRICS)
			|| (_type == MidiEvent::META_MARKER)
			|| (_type == MidiEvent::META_CUE);
	}

	bool MidiEvent::is_key() const {
		return (_type == MidiEvent::MSG_NOTE_OFF)
			|| (_type == MidiEvent::MSG_NOTE_ON)
			|| (_type == MidiEvent::MSG_KEY_PRESS);
	}

	int MidiEvent::channel() const {
		if (!(is_meta() || is_sysex())) return _channel;
		return -1;
	}

	const uint8_t* MidiEvent::data() const {
		return _data;
	}

	uint8_t MidiEvent::data(size_t index) const {
		if (index >= _size)
			throw std::out_of_range(
			"Attempt to read outside of memory"
			);
		return _data[index];
	}

	size_t MidiEvent::offset() const {
		return _offset;
	}

	MidiEvent::EventType MidiEvent::type() const {
		return (EventType)_type;
	}

	size_t MidiEvent::size() const {
		if (is_meta()) return _size;
		switch (_type) {
		case MidiEvent::MSG_NOTE_OFF: return 2;
		case MidiEvent::MSG_NOTE_ON: return 2;
		case MidiEvent::MSG_KEY_PRESS: return 2;
		case MidiEvent::MSG_CONTROLLER: return 2;
		case MidiEvent::MSG_PROGRAM: return 1;
		case MidiEvent::MSG_CHANNEL: return 1;
		case MidiEvent::MSG_PITCH: return 2;

		case MidiEvent::SYSEX: return _size;
		case MidiEvent::SYSEX_POSITION: return 2;
		case MidiEvent::SYSEX_SELECT: return 1;
		case MidiEvent::SYSEX_TUNE: return 0;
		case MidiEvent::SYSEX_NEXT: return _size;
		case MidiEvent::SYSEX_TIMING: return 0;
		case MidiEvent::SYSEX_START: return 0;
		case MidiEvent::SYSEX_CONTINUE: return 0;
		case MidiEvent::SYSEX_STOP: return 0;
		case MidiEvent::SYSEX_ACTIVE: return 0;
		}
		return _size;
	}

	uint8_t MidiEvent::key() const {
		return is_key()
			? data(0)
			: 0xff;
	}

	size_t MidiEvent::value() const {
		switch (_type) {
		case MidiEvent::MSG_NOTE_OFF: return _data[1];
		case MidiEvent::MSG_NOTE_ON: return _data[1];
		case MidiEvent::MSG_KEY_PRESS: return _data[1];
		case MidiEvent::MSG_CONTROLLER: return _data[1];
		case MidiEvent::MSG_CHANNEL: return _data[0];
		case MidiEvent::MSG_PITCH: return (_data[0] << 7) | (_data[1]);
		case MidiEvent::META_CH_PREFIX: return _data[0];
		case MidiEvent::META_MIDI_PORT: return _data[0];
		case MidiEvent::META_SET_TEMPO: return (_data[0] << 16) | (_data[1] << 8) | (_data[2]);
		}
		return -1;
	}

	void MidiEvent::clear() {
		_channel = 0;
		_size = 0;
		if (_data) delete[] _data;
		_data = nullptr;
	}

	size_t MidiEvent::getShortMessage() const {
		if (is_meta() || is_sysex()) return 0;
		return (unsigned(_data[0]) << 8)
			| (unsigned(_data[1]) << 16)
			| (_type & 0xf0) 
			| (_channel & 0x0f);
	}

	size_t MidiEvent::read(uint8_t* buf) {
		static uint16_t prev_type = 0;
		static uint8_t prev_channel = 0;
		clear();
		uint8_t* start = buf;
		buf += read_uint32_vlen(_offset, buf);
		uint8_t t = *buf;
		if (!(t & 0x80)) {
			if (!prev_type) throw rw_error(
				"Invalid type", buf
				);
			_type = prev_type;
			_channel = prev_channel;
			if (is_meta() || is_sysex()) {
				buf += read_uint32_vlen(_size, buf);
			} else {
				_size = size();
			}
		} else if (t == 0xff) {
			buf += read_uint16_be(_type, buf);
			buf += read_uint32_vlen(_size, buf);
		} else if ((t & 0xf0) == 0xf0) {
			_type = t;
			++buf;
			buf += read_uint32_vlen(_size, buf);
		} else {
			_type = t & 0xf0;
			_channel = t & 0x0f;
			++buf;
			_size = size();
		}
		prev_type = _type;
		prev_channel = _channel;
		if (_size) {
			_data = new uint8_t[_size];
			memcpy(_data, buf, _size);
			buf += _size;
		}
		return buf - start;
	}

	size_t MidiEvent::write(uint8_t* buf) {
		static uint16_t prev_type = 0;
		uint8_t* start = buf;
		buf += write_uint32_vlen(_offset, buf);
		uint16_t type = _type;
		if (!is_meta() && !is_sysex()) {
			type |= _channel;
		}
		if (is_meta()) {
			buf += write_uint16_be(type, buf);
		} else if (!is_sysex() && type != prev_type) {
			*buf++ = type & 0xff;
		}
		prev_type = type;
		if (is_meta() || is_sysex()) {
			buf += write_uint32_vlen(_size, buf);
		}
		if (_size) {
			memcpy(buf, _data, _size);
			buf += _size;
		}
		return buf - start;
	}

	const char* MidiEvent::event_name() const {
		switch (_type) {
		case MidiEvent::MSG_NOTE_OFF: return "Note off";
		case MidiEvent::MSG_NOTE_ON: return "Note on";
		case MidiEvent::MSG_KEY_PRESS: return "Polyphonic key pressure";
		case MidiEvent::MSG_CONTROLLER: return "Controller change";
		case MidiEvent::MSG_PROGRAM: return "Program change";
		case MidiEvent::MSG_CHANNEL: return "Channel pressure";
		case MidiEvent::MSG_PITCH: return "Pitch change";
		case MidiEvent::SYSEX: return "System exclusive begin";
		case MidiEvent::SYSEX_NEXT: return "System exclusive end";
		case MidiEvent::META_SEQUENCE: return "Sequence number";
		case MidiEvent::META_TEXT: return "Text";
		case MidiEvent::META_COPYRIGHT: return "Copyrights";
		case MidiEvent::META_TRACK_NAME: return "Track name";
		case MidiEvent::META_INSTRUMENT: return "Instrument name";
		case MidiEvent::META_LYRICS: return "Lyrics";
		case MidiEvent::META_MARKER: return "Marker";
		case MidiEvent::META_CUE: return "Cue point";
		case MidiEvent::META_CH_PREFIX: return "Forced MIDI channel";
		case MidiEvent::META_MIDI_PORT: return "Forced MIDI port";
		case MidiEvent::META_END_TRACK: return "End of track";
		case MidiEvent::META_SET_TEMPO: return "Set tempo";
		case MidiEvent::META_OFFSET: return "SMPTE offset";
		case MidiEvent::META_TIME_SIGN: return "Time signature";
		case MidiEvent::META_KEY_SIGN: return "Key signature";
		case MidiEvent::META_SPECIAL: return "Sequencer specific";
		}
		return "<Unknown event>";
	}

	const char* MidiEvent::note_name() const {
		static const char* notes[] = {
			"C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B"
		};
		static const char* percussions[] = {
			"Acoustic Bass Drum",
			"Bass Drum 1",
			"Side Stick",
			"Acoustic Snare",
			"Hand Clap",
			"Electric Snare",
			"Low Floor Tom",
			"Closed Hi-Hat",
			"High Floor Tom",
			"Pedal Hi-Hat",
			"Low Tom",
			"Open Hi-Hat",
			"Low-Mid Tom",
			"Hi-Mid Tom",
			"Crash Cymbal 1",
			"High Tom",
			"Ride Cymbal 1",
			"Chinese Cymbal",
			"Ride Bell",
			"Tambourine",
			"Splash Cymbal",
			"Cowbell",
			"Crash Cymbal 2",
			"Vibraslap",
			"Ride Cymbal 2",
			"Hi Bongo",
			"Low Bongo",
			"Mute Hi Conga",
			"Open Hi Conga",
			"Low Conga",
			"High Timbale",
			"Low Timbale",
			"High Agogo",
			"Low Agogo",
			"Cabasa",
			"Maracas",
			"Short Whistle",
			"Long Whistle",
			"Short Guiro",
			"Long Guiro",
			"Claves",
			"Hi Wood Block",
			"Low Wood Block",
			"Mute Cuica",
			"Open Cuica",
			"Mute Triangle",
			"Open Triangle"
		};
		uint8_t k = key();
		if (k > 127) return "<Not a note>";
		if (_channel == PERCUSSION_CHANNEL) {
			if ((k < 35) || (k > 81)) return "<Not a percussion>";
			return percussions[k - 35];
		}
		k %= 12;
		return notes[k];
	}

	int MidiEvent::octave() const {
		int k = key();
		if (k > 127) return INT_MAX;
		return (k % 12) - 1;
	}

	const char* MidiEvent::instrument_family() const {
		if ((_type != MSG_PROGRAM)) return "<No instrument>";
		// if (_channel == PERCUSSION_CHANNEL) return "Percussion channel";
		uint8_t instrument = _data[0];
		static const char* families[] = {
			"Piano",
			"Chromatic percussion",
			"Organ",
			"Guitar",
			"Bass",
			"Strings",
			"Ensemble",
			"Brass",
			"Reed",
			"Pipe",
			"Synth lead",
			"Synth pad",
			"Synth effects",
			"Ethnic",
			"Percussive",
			"Sound effects"
		};
		if (instrument > 127) return "<Not an instrument>";
		return families[instrument >> 3];
	}

	const char* MidiEvent::instrument_name() const {
		if ((_type != MSG_PROGRAM)) return "<No instrument>";
		// if (_channel == PERCUSSION_CHANNEL) return "Percussion channel";
		uint8_t instrument = _data[0];
		static const char* instruments[] = {
			"Acoustic Grand",
			"Bright Acoustic",
			"Electric Grand",
			"Honky-Tonk",
			"Electric Piano 1 (Rhodes piano)",
			"Electric Piano 2 (Chorused piano)",
			"Harpsichord",
			"Clav",
			"Celesta",
			"Glockenspiel",
			"Music Box",
			"Vibraphone",
			"Marimba",
			"Xylophone",
			"Tubular Bells",
			"Dulcimer (Santur)",
			"Drawbar Organ (Hammond)",
			"Percussive Organ",
			"Rock Organ",
			"Church Organ",
			"Reed Organ",
			"Accoridan (French)",
			"Harmonica",
			"Tango Accordian (Band neon)",
			"Acoustic Guitar (nylon)",
			"Acoustic Guitar (steel)",
			"Electric Guitar (jazz)",
			"Electric Guitar (clean)",
			"Electric Guitar (muted)",
			"Overdriven Guitar",
			"Distortion Guitar",
			"Guitar Harmonics",
			"Acoustic Bass",
			"Electric Bass (finger)",
			"Electric Bass (pick)",
			"Fretless Bass",
			"Slap Bass 1",
			"Slap Bass 2",
			"Synth Bass 1",
			"Synth Bass 2",
			"Violin",
			"Viola",
			"Cello",
			"Contrabass",
			"Tremolo Strings",
			"Pizzicato Strings",
			"Orchestral Strings",
			"Timpani",
			"String Ensemble 1 (strings)",
			"String Ensemble 2 (slow strings)",
			"SynthStrings 1",
			"SynthStrings 2",
			"Choir Aahs",
			"Voice Oohs",
			"Synth Voice",
			"Orchestra Hit",
			"Trumpet",
			"Trombone",
			"Tuba",
			"Muted Trumpet",
			"French Horn",
			"Brass Section",
			"SynthBrass 1",
			"SynthBrass 2",
			"Soprano Sax",
			"Alto Sax",
			"Tenor Sax",
			"Baritone Sax",
			"Oboe",
			"English Horn",
			"Bassoon",
			"Clarinet",
			"Piccolo",
			"Flute",
			"Recorder",
			"Pan Flute",
			"Blown Bottle",
			"Skakuhachi",
			"Whistle",
			"Ocarina",
			"Lead 1 (square)",
			"Lead 2 (sawtooth)",
			"Lead 3 (calliope)",
			"Lead 4 (chiff)",
			"Lead 5 (charang)",
			"Lead 6 (voice)",
			"Lead 7 (fifths)",
			"Lead 8 (bass+lead)",
			"Pad 1 (new age)",
			"Pad 2 (warm)",
			"Pad 3 (polysynth)",
			"Pad 4 (choir)",
			"Pad 5 (bowed)",
			"Pad 6 (metallic)",
			"Pad 7 (halo)",
			"Pad 8 (sweep)",
			"FX 1 (rain)",
			"FX 2 (soundtrack)",
			"FX 3 (crystal)",
			"FX 4 (atmosphere)",
			"FX 5 (brightness)",
			"FX 6 (goblins)",
			"FX 7 (echoes)",
			"FX 8 (sci-fi)",
			"Sitar",
			"Banjo",
			"Shamisen",
			"Koto",
			"Kalimba",
			"Bagpipe",
			"Fiddle",
			"Shanai",
			"Tinkle Bell",
			"Agogo",
			"Steel Drums",
			"Woodblock",
			"Taiko Drum",
			"Melodic Tom",
			"Synth Drum",
			"Reverse Cymbal",
			"Guitar Fret Noise",
			"Breath Noise",
			"Seashore",
			"Bird Tweet",
			"Telephone Ring",
			"Helicopter",
			"Applause",
			"Gunshot"
		};
		if (instrument > 127) return "<Not an instrument>";
		return instruments[instrument];
	}

	const char* MidiEvent::controller_name() const {
		if ((_type != MSG_CONTROLLER)) return "<No controller>";
		switch (_data[0]) {
		case 0x00: return "Blank Select (MSB)";
		case 0x01: return "Modulation Wheel (MSB)";
		case 0x02: return "Breath Control (MSB)";
		case 0x04: return "Foot Controller (MSB)";
		case 0x05: return "Portamento Time (MSB)";
		case 0x06: return "Data Entry (MSB)";
		case 0x07: return "Channel Volume (MSB)";
		case 0x08: return "Balance (MSB)";
		case 0x0a: return "Pan (MSB)";
		case 0x0b: return "Expression Controller (MSB)";
		case 0x0c: return "Effect control 1 (MSB)";
		case 0x0d: return "Effect control 2 (MSB)";

		case 0x10: return "General Purpose Controller #1 (MSB)";
		case 0x11: return "General Purpose Controller #2 (MSB)";
		case 0x12: return "General Purpose Controller #3 (MSB)";
		case 0x13: return "General Purpose Controller #4 (MSB)";

		case 0x20: return "Blank Select (LSB)";
		case 0x21: return "Modulation Wheel (LSB)";
		case 0x22: return "Breath Control (LSB)";
		case 0x24: return "Foot Controller (LSB)";
		case 0x25: return "Portamento Time (LSB)";
		case 0x26: return "Data Entry (LSB)";
		case 0x27: return "Channel Volume (LSB)";
		case 0x28: return "Balance (LSB)";
		case 0x2a: return "Pan (LSB)";
		case 0x2b: return "Expression Controller (LSB)";
		case 0x2c: return "Effect control 1 (LSB)";
		case 0x2d: return "Effect control 2 (LSB)";

		case 0x30: return "General Purpose Controller #1 (LSB)";
		case 0x31: return "General Purpose Controller #2 (LSB)";
		case 0x32: return "General Purpose Controller #3 (LSB)";
		case 0x33: return "General Purpose Controller #4 (LSB)";

		case 0x40: return "Damper pedal (on/off)";
		case 0x41: return "Portamento (on/off)";
		case 0x42: return "Sustenuto (on/off)";
		case 0x43: return "Soft pedal (on/off)";
		case 0x44: return "Legato Footswitch (on/off)";
		case 0x45: return "Hold 2 (on/off)";
		case 0x46: return "Sound Variation";
		case 0x47: return "Timbre";
		case 0x48: return "Release Time";
		case 0x49: return "Attack Time";
		case 0x4a: return "Brightness";
		case 0x4b: return "Sound Controller 6";
		case 0x4c: return "Sound Controller 7";
		case 0x4d: return "Sound Controller 8";
		case 0x4e: return "Sound Controller 9";
		case 0x4f: return "Sound Controller 10";

		case 0x50: return "General Purpose Controller #5";
		case 0x51: return "General Purpose Controller #6";
		case 0x52: return "General Purpose Controller #7";
		case 0x53: return "General Purpose Controller #8";
		case 0x54: return "Portamento Control";
		case 0x5b: return "Effects 1 Depth";
		case 0x5c: return "Effects 2 Depth";
		case 0x5d: return "Effects 3 Depth";
		case 0x5e: return "Effects 4 Depth";
		case 0x5f: return "Effects 5 Depth";

		case 0x60: return "Data Entry +1";
		case 0x61: return "Data Entry -1";
		case 0x62: return "Non-Registered Parameter Number (LSB)";
		case 0x63: return "Non-Registered Parameter Number (MSB)";
		case 0x64: return "Registered Parameter Number (LSB)";
		case 0x65: return "Registered Parameter Number (MSB)";

		case 0x78: return "All Sound Off";
		case 0x79: return "Reset All Controllers";
		case 0x7a: return "Local control (on/off)";
		case 0x7b: return "All notes off";
		case 0x7c: return "Omni mode off";
		case 0x7d: return "Omni mode on";
		case 0x7e: return "Poly mode on/off";
		case 0x7f: return "Poly mode on (mono off + all notes off)";
		}
		return "<Unknown controller>";
	}

	MidiEvent MidiEvent::NoteOn(uint8_t channel, uint8_t note, uint8_t velocity) {
		if (note & 0x80) throw std::out_of_range(
			"Wrong note index"
			);
		if (velocity & 0x80) throw std::out_of_range(
			"Wrong velocity value"
			);
		MidiEvent mevt(2);
		mevt._type = MidiEvent::MSG_NOTE_ON;
		return mevt.channel(channel).data(0, note).data(1, velocity);
	}

	MidiEvent MidiEvent::NoteOff(uint8_t channel, uint8_t note, uint8_t velocity) {
		if (note & 0x80) throw std::out_of_range(
			"Wrong note index"
			);
		if (velocity & 0x80) throw std::out_of_range(
			"Wrong velocity value"
			);
		MidiEvent mevt(2);
		mevt._type = MidiEvent::MSG_NOTE_OFF;
		return mevt.channel(channel).data(0, note).data(1, velocity);
	}

	MidiEvent MidiEvent::KeyPress(uint8_t channel, uint8_t note, uint8_t value) {
		if (note & 0x80) throw std::out_of_range(
			"Wrong note index"
			);
		if (value & 0x80) throw std::out_of_range(
			"Wrong value"
			);
		MidiEvent mevt(2);
		mevt._type = MidiEvent::MSG_KEY_PRESS;
		return mevt.channel(channel).data(0, note).data(1, value);
	}

	MidiEvent MidiEvent::Controller(uint8_t channel, uint8_t controller, uint8_t value) {
		if (controller & 0x80) throw std::out_of_range(
			"Wrong controller index"
			);
		if (value & 0x80) throw std::out_of_range(
			"Wrong value"
			);
		MidiEvent mevt(2);
		mevt._type = MidiEvent::MSG_CONTROLLER;
		return mevt.channel(channel).data(0, controller).data(1, value);
	}

	MidiEvent MidiEvent::Instrument(uint8_t channel, uint8_t instrument) {
		if (instrument & 0x80) throw std::out_of_range(
			"Wrong instrument index"
			);
		MidiEvent mevt(1);
		mevt._type = MidiEvent::MSG_PROGRAM;
		return mevt.channel(channel).data(0, instrument);
	}
	
	MidiEvent MidiEvent::ChannelPressure(uint8_t channel, uint8_t pressure) {
		if (pressure & 0x80) throw std::out_of_range(
			"Wrong pressure value"
			);
		MidiEvent mevt(1);
		mevt._type = MidiEvent::MSG_CHANNEL;
		return mevt.channel(channel).data(0, pressure);
	}

	MidiEvent MidiEvent::Pitch(uint8_t channel, uint16_t pitch) {
		if (pitch & 0xc000) throw std::out_of_range(
			"Wrong pitch value"
			);
		MidiEvent mevt(2);
		mevt._type = MidiEvent::MSG_PITCH;
		return mevt.channel(channel).data(0, pitch & 0x7f).data(1, pitch >> 7);
	}

	MidiEvent MidiEvent::Tempo(size_t tempo) {
		if (!tempo || (tempo & 0xff000000))throw std::out_of_range(
			"Wrong tempo value"
			);
		MidiEvent mevt(3);
		mevt._type = MidiEvent::META_SET_TEMPO;
		return mevt.data(0, (tempo >> 16) & 0xff).data(1, (tempo >> 8) & 0xff).data(2, tempo & 0xff);
	}

	MidiEvent MidiEvent::EndOfTrack() {
		MidiEvent mevt(0);
		mevt._type = MidiEvent::META_END_TRACK;
		return mevt;
	}

	uint8_t note_by_octave_key(int8_t octave, uint8_t key) {
		if ((octave < -1) || (octave > 9)) return 0;
		if (key > 12) return 0;
		return ((octave + 1) * 12 + key) & 0x7f;
	}

	size_t tempo2bpm(size_t tempo) {
		return 60000000u / tempo;
	}

	size_t bpm2tempo(size_t bpm) {
		return 60000000u / bpm;
	}

}