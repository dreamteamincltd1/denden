#include "../include/integers.h"
#include "../include/rw_error.h"

#include <stdexcept>

size_t read_uint16_be(uint16_t& data, uint8_t* buf) {
	data = (buf[0] << 8) | buf[1];
	return 2;
}

size_t read_uint32_be(uint32_t& data, uint8_t* buf) {
	data = (buf[0] << 24) | (buf[1] << 16) | (buf[2] << 8) | buf[3];
	return 4;
}

size_t read_uint32_vlen(uint32_t& data, uint8_t* buf) {
	data = 0;
	uint8_t* start = buf;
	do {
		data = (data << 7) | (*buf & 0x7f);
		if (data & 0xf0000000) throw rw_error(
			"Variable length exceeds maximum", buf
			);
	} while (*buf++ & 0x80);
	return buf - start;
}

size_t write_uint16_be(uint16_t data, uint8_t* buf) {
	buf[0] = (data >> 8) & 0xff;
	buf[1] = data & 0xff;
	return 2;
}

size_t write_uint32_be(uint32_t data, uint8_t* buf) {
	buf[0] = (data >> 24) & 0xff;
	buf[1] = (data >> 16) & 0xff;
	buf[2] = (data >> 8) & 0xff;
	buf[3] = data & 0xff;
	return 4;
}

size_t write_uint32_vlen(uint32_t data, uint8_t* buf) {
	if (data & 0xf0000000) throw rw_error(
		"Variable length exceeds maximum", buf
		);
	uint8_t* start = buf;
	bool skip = true;
	for (int mask_pos = 3; mask_pos >= 0; --mask_pos) {
		size_t mask = 0x7f << (mask_pos*7);
		if (skip && mask_pos && !(data & mask)) continue;
		skip = false;
		uint8_t byte = (data & mask) >> (mask_pos * 7);
		byte |= mask_pos ? 0x80 : 0;
		*buf++ = byte;
	}
	return buf - start;
}
