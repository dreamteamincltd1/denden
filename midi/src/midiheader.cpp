#include "../include/integers.h"
#include "../include/midiheader.h"
#include "../include/rw_error.h"

#include <memory>
#include <stdexcept>

namespace midi {
	const char MidiHeader::_SIGNATURE[4] = { 'M', 'T', 'h', 'd' };

	size_t MidiHeader::tracks_count() const {
		return _n_tracks;
	}

	size_t MidiHeader::ticks_per_quarter_note() const {
		return _ticks_per_qtr_note;
	}

	MidiHeader::TrackFormat MidiHeader::format() const {
		if (_n_tracks == 1) return SINGLE_TRACK;
		if (!_async) return MULTI_TRACK;
		return MULTI_TRACK_ASYNC;
	}

	size_t MidiHeader::read(uint8_t* buf) {
		uint8_t* start = buf;
		if (memcmp(buf, _SIGNATURE, 4)) throw rw_error(
			"Invalid header signature", buf
			);
		buf += 4;
		uint32_t len;
		buf += read_uint32_be(len, buf);
		if (len != _LENGTH) throw rw_error(
			"Unsupported header length", buf
			);
		uint16_t format;
		buf += read_uint16_be(format, buf);
		if (format > 3) throw rw_error(
			"Unsupported tracks format", buf
			);
		_async = (format == MULTI_TRACK_ASYNC);
		buf += read_uint16_be(_n_tracks, buf);
		if (!_n_tracks
			|| ((format == SINGLE_TRACK) && (_n_tracks != 1))
			) throw std::invalid_argument(
			"Wrong tracks number"
			);
		uint16_t time;
		buf += read_uint16_be(time, buf);
		if (time & 0x8000) {
			_frames_per_second = ~((time >> 8) & 0xff) + 1;
			_resolution = time & 0xff;
			_ticks_per_qtr_note = _frames_per_second * _resolution;
		} else {
			_frames_per_second = 0;
			_resolution = 0;
			_ticks_per_qtr_note = time;
		}
		if (!_ticks_per_qtr_note) throw std::invalid_argument(
			"Invalid time format"
			);
		return buf - start;
	}

	size_t MidiHeader::write(uint8_t* buf) {
		uint8_t* start = buf;
		memcpy(buf, _SIGNATURE, 4);
		buf += 4;
		buf += write_uint32_be(_LENGTH, buf);
		buf += write_uint16_be(format(), buf);
		buf += write_uint16_be(_n_tracks, buf);
		uint16_t time = _ticks_per_qtr_note;
		if (_frames_per_second) {
			time = ((-_frames_per_second) << 8) | (_resolution & 0xff);
		}
		buf += write_uint16_be(time, buf);
		return buf - start;
	}

	MidiHeader MidiHeader::SingleTrack(uint16_t ticks, uint8_t fps) {
		MidiHeader mh;
		mh._n_tracks = 1;
		mh._async = false;
		mh._ticks_per_qtr_note = ticks;
		mh._frames_per_second = fps;
		if (fps) {
			mh._resolution = ticks / fps;
		} else {
			mh._resolution = 0;
		}
		return mh;
	}

	MidiHeader MidiHeader::MultiTrack(size_t tracks, uint16_t ticks, uint8_t fps) {
		if (!tracks) throw std::invalid_argument(
			"Wrong tracks number"
			);
		MidiHeader mh;
		mh._async = false;
		mh._n_tracks = tracks;
		mh._ticks_per_qtr_note = ticks;
		mh._frames_per_second = fps;
		if (fps) {
			mh._resolution = ticks / fps;
		} else {
			mh._resolution = 0;
		}
		return mh;
	}

	void MidiHeader::ticks_per_quarter_note(size_t ticks) {
		if (!ticks || (ticks & 0xffff8000)) throw std::invalid_argument(
			"Wrong ticks number"
			);
		_ticks_per_qtr_note = ticks;
		if (_frames_per_second) {
			_resolution = _ticks_per_qtr_note / _frames_per_second;
		} 
	}


}