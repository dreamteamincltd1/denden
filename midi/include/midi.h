#pragma once
#ifndef MIDI_H
#define MIDI_H

#include "midiheader.h"
#include "miditrack.h"
#include <cstdio>
#include <vector>

namespace midi {
	class Midi {
	public:
		//void Play();
		size_t read(uint8_t* buf);
		size_t write(uint8_t* buf);
		void print_info(FILE* fp, unsigned verbose_level = 0);

		size_t ticks_per_quarter_note() const;
		void ticks_per_quarter_note(size_t ticks);

		std::vector<MidiTrack> tracks;
		typedef std::vector<MidiTrack>::iterator TrackIterator;

		Midi merge() const;

		void setHeader(const MidiHeader& header);
	private:
		MidiHeader _header;
	};
}
#endif // MIDI_H