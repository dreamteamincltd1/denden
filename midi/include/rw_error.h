#pragma once
#ifndef RW_ERROR_H
#define RW_ERROR_H
#include <stdexcept>
#include <cstdint>

class rw_error : public std::invalid_argument {
public:
	rw_error(const char* what, uint8_t* where);
	rw_error(const std::string& what, uint8_t* where);
	uint8_t* where() const;
protected:
	uint8_t* _where;
};

#endif // RW_ERROR_H