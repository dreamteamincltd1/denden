#pragma once
#ifndef MIDITRACK_H
#define MIDITRACK_H
#include "midievent.h"
#include <cstdint>
#include <vector>

namespace midi {
	class MidiTrack {
	public:
		MidiTrack();
		MidiTrack(size_t tracks);

		size_t size() const;
		size_t read(uint8_t* buf);
		size_t write(uint8_t* buf);

		void size(const size_t);

		std::vector<MidiEvent> events;
		typedef std::vector<MidiEvent>::iterator EventIterator;

	private:
		size_t _length;
		static const char _SIGNATURE[4];
	};
}
#endif // MIDITRACK_H