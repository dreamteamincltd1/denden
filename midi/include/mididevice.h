#pragma once
#ifndef MIDI_DEVICE_H
#define MIDI_DEVICE_H
#include "midievent.h"
#include <Windows.h>
#include <mmsystem.h>

namespace midi {
	class MidiDevice {
	public:
		MidiDevice(bool precise = false);
		~MidiDevice();

		// Plays midi event
		// If delayed is false - signal sent immidiately (For real time event)
		// Otherwise event starts after offset in midi event (For playing tracks)
		void play(const MidiEvent& me, bool delayed = false);
		
		// Stops all sound & reset counters 
		void stop();

		size_t tempo() const;
		void tempo(size_t);

		size_t ticks_per_quarter_note() const;
		void ticks_per_quarter_note(size_t);

		void sleep_tacts(size_t t);

	private:
		bool _precise;
		bool _playing;
		HMIDIOUT _out;
		size_t _tempo;
		size_t _ticks;
		LARGE_INTEGER _tick;
		LARGE_INTEGER _cpu_freq;
	};
}
#endif MIDI_DEVICE_H