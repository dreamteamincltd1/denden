#pragma once
#ifndef MIDIHEADER_H
#define MIDIHEADER_H
#include <cstdint>

namespace midi {
	class MidiHeader {
	public:

		/*	MidiHeader();
			MidiHeader(const uint16_t tracks,
			const uint8_t ticks,
			bool async = false);
			MidiHeader(const uint16_t tracks,
			const uint8_t fps,
			const uint8_t resolution,
			bool async = false);
			*/
		static MidiHeader SingleTrack(uint16_t ticks, uint8_t fps=0);
		static MidiHeader MultiTrack(size_t tracks, uint16_t ticks, uint8_t fps=0);

		enum TrackFormat {
			SINGLE_TRACK = 0,
			MULTI_TRACK = 1,
			MULTI_TRACK_ASYNC = 2
		};

		TrackFormat format() const;
		size_t tracks_count() const;
		size_t ticks_per_quarter_note() const;
		void ticks_per_quarter_note(size_t ticks);

		size_t read(uint8_t* buf);
		size_t write(uint8_t* buf);

	private:
		static const char _SIGNATURE[4];
		static const uint32_t _LENGTH = 6;
		uint16_t _n_tracks;
		uint16_t _ticks_per_qtr_note;
		uint8_t _frames_per_second;
		uint8_t _resolution;
		bool _async;
	};
}
#endif // MIDIHEADER_H