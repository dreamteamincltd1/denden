#pragma once
#ifndef MIDIEVENT_H
#define MIDIEVENT_H
#include <cstdint>

namespace midi {
	class MidiEvent {
	public:
		enum EventType {
			// - MIDI MESSAGE EVENTS -
			MSG_NOTE_OFF = 0x80,     // Note off event              [....channel][.key][.velocity]
			MSG_NOTE_ON = 0x90,      // Note on event               [....channel][.key][.velocity]
			MSG_KEY_PRESS = 0xa0,    // Polyphonic key pressure     [....channel][.key][.value]
			MSG_CONTROLLER = 0xb0,   // Control change              [....channel][.controller][.value]
			MSG_PROGRAM = 0xc0,      // Program change              [....channel][.program]
			MSG_CHANNEL = 0xd0,      // Channel pressure            [....channel][.value]
			MSG_PITCH = 0xe0,        // Pitch wheel change          [....channel][.pitch-lo][.pitch-hi]

			// - SYSTEM COMMON EVENTS -
			SYSEX = 0xf0,             // System exclusive message   <F0>{length}[.id][.data]*<F7>?
			SYSEX_POSITION = 0xf2,    // Song position pointer      <F2>[.beats-lo][.beats-hi]
			SYSEX_SELECT = 0xf3,      // Song select                <F3>[.song]
			SYSEX_TUNE = 0xf6,        // Tune request               <F6>
			SYSEX_NEXT = 0xf7,        // Part of sysex message      <F7>{length}[.id][.data]*<F7>?
			SYSEX_TIMING = 0xf8,      // Timing clock               <F8>
			SYSEX_START = 0xfa,       // Start playing              <FA>
			SYSEX_CONTINUE = 0xfb,    // Continue playing           <FB>
			SYSEX_STOP = 0xfc,        // Stop playing               <FC>
			SYSEX_ACTIVE = 0xfe,      // Active Sensing mode        <FE>

			// - META EVENTS -
			META = 0xff,              // Meta event signature       <FF>[.type]{length}data*
			META_SEQUENCE = 0xff00,   // Sequence number
			META_TEXT = 0xff01,       // Text event
			META_COPYRIGHT = 0xff02,  // Copyright notice
			META_TRACK_NAME = 0xff03, // Name of track
			META_INSTRUMENT = 0xff04, // Instrument name
			META_LYRICS = 0xff05,     // Lyrics
			META_MARKER = 0xff06,     // Marker
			META_CUE = 0xff07,        // Cue point
			META_CH_PREFIX = 0xff20,  // Channel prefix             [channel]
			META_MIDI_PORT = 0xff21,  // Midi port                  [port]
			META_END_TRACK = 0xff2f,  // End of track
			META_SET_TEMPO = 0xff51,  // Set tempo (ms/q.note)      [tempo-hi][tempo-md][tempo-lo]
			META_OFFSET = 0xff54,     // SMPTE offset               [hour][min][sec][frame][fract]
			META_TIME_SIGN = 0xff58,  // Time signature             [nom][denom][clocks][notes]
			META_KEY_SIGN = 0xff59,   // Key signature              [shift][minor]
			META_SPECIAL = 0xff7f     // Sequencer specific
		};

		MidiEvent();
		explicit MidiEvent(size_t size);
		MidiEvent(const MidiEvent&);
		void operator=(const MidiEvent&);
		~MidiEvent();

		MidiEvent& offset(size_t offset_);
		MidiEvent& channel(uint8_t channel_);
		MidiEvent& data(size_t index, uint8_t value_);

		EventType type() const;
		size_t size() const;
		const uint8_t* data() const;
		uint8_t data(size_t index) const;
		size_t offset() const;
		int channel() const;
		uint8_t key() const;
		size_t value() const;

		bool is_sysex() const;
		bool is_meta() const;
		bool is_text() const;
		bool is_key() const;

		size_t read(uint8_t* buf);
		size_t write(uint8_t* buf);

		void clear();

		const char* event_name() const;
		const char* note_name() const;
		int octave() const;
		const char* instrument_family() const;
		const char* instrument_name() const;
		const char* controller_name() const;

		static const int PERCUSSION_CHANNEL = 9;

		// Get message to send to midi out
		size_t getShortMessage() const;


		static MidiEvent NoteOn(uint8_t channel, uint8_t note, uint8_t velocity = 0x7f);
		static MidiEvent NoteOff(uint8_t channel, uint8_t note, uint8_t velocity = 0x7f);
		static MidiEvent KeyPress(uint8_t channel, uint8_t note, uint8_t value = 0x7f);
		static MidiEvent Controller(uint8_t channel, uint8_t controller, uint8_t value);
		static MidiEvent Instrument(uint8_t channel, uint8_t instrument);
		static MidiEvent ChannelPressure(uint8_t channel, uint8_t pressure);
		static MidiEvent Pitch(uint8_t channel, uint16_t pitch = 0x2000);
		static MidiEvent Tempo(size_t tempo);
		static MidiEvent EndOfTrack();

	private:
		size_t _offset;
		uint16_t _type;
		uint8_t _channel;
		uint8_t* _data;
		size_t _size;

	};

	uint8_t note_by_octave_key(int8_t octave, uint8_t key);

	size_t tempo2bpm(size_t tempo);
	size_t bpm2tempo(size_t bpm);

}
#endif // MIDIEVENT_H