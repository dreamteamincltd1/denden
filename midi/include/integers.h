#pragma once
#ifndef INTEGERS_H
#define INTEGERS_H
#include <cstdint>

size_t read_uint16_be(uint16_t& data, uint8_t* buf);
size_t read_uint32_be(uint32_t& data, uint8_t* buf);
size_t read_uint32_vlen(uint32_t& data, uint8_t* buf);

size_t write_uint16_be(uint16_t data, uint8_t* buf);
size_t write_uint32_be(uint32_t data, uint8_t* buf);
size_t write_uint32_vlen(uint32_t data, uint8_t* buf);

#endif // INTEGERS_H