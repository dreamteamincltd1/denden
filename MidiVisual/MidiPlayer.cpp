#include "MidiPlayer.h"


MidiPlayer::MidiPlayer(MidiProcessor * processor)
{
	this->processor = processor;
}


MidiPlayer::~MidiPlayer()
{
}

void MidiPlayer::play(midi::MidiEvent me) {
	processor->midiDevice->play(me, 0);
	emit resultReady();
}
