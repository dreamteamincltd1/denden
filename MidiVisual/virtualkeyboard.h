#ifndef VIRTUALKEYBOARD_H
#define VIRTUALKEYBOARD_H

#include <QGraphicsScene>
#include <QGraphicsView>
#include <QPushButton>
#include <QShortcut>
#include "midikey.h"
#include "iostream" ///!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

class VirtualKeyboard : public QGraphicsScene
{
	Q_OBJECT
		friend class MidiVisual;
public:
	VirtualKeyboard(QGraphicsView *view, QObject *parent);
	~VirtualKeyboard();
private:
	MidiKey* pkeysW[14];
	MidiKey* pkeysB[10];
	QString *keysNamesB;
	int *keysNumB;
	QString *keysNamesW;
	int *keysNumW;
signals:
	void keyPressedNum(int num);
	void keyReleasedNum(int num);
};

#endif // VIRTUALKEYBOARD_H
