#ifndef INSTRUMENTSBOX_H
#define INSTRUMENTSBOX_H

#include <QComboBox>
#include <QObject>
#include <QStringList>
class InstrumentsBox : public QComboBox
{
	Q_OBJECT

public:
	InstrumentsBox(QWidget *parent);
	~InstrumentsBox();
	void showInstrumentsFromeFamily();
	QString *instruments;
	QStringList *instrInSelectedFamily;
	int familyNum;//0..14
	int instrumentNum; //0..7
public slots:
	void familyChanged(int  family);
	void myValueChanged(int instrument);
	void octaveChanged(int octave);
signals:
	void instrumentChanged(int instrument);
private:
	
};

#endif // INSTRUMENTSBOX_H
