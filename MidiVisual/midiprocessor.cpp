﻿#include "midiprocessor.h"
#include "midivisual.h"
#include <iostream>

MidiProcessor::MidiProcessor(QObject *parent)
	: QObject(parent)
{
//	threadForPlaying = new QThread;
	recording = 0;
	octave = 0;
	instrument = 0;
	percussion = 0;
	fadeIn = 127;
	fadeOut = 127;
	perc_on = midi::MidiEvent::NoteOn(9, 35);
    perc_off = midi::MidiEvent::NoteOff(9, 35);
}
MidiProcessor::MidiProcessor()	: QObject()
{
	//threadForPlaying = new QThread;
	recording = 0;
	octave = 0;
	instrument = 0;
	percussion = 0;
	fadeIn = 127;
	fadeOut = 127;
	perc_on = midi::MidiEvent::NoteOn(9, 35);
	perc_off = midi::MidiEvent::NoteOff(9, 35);
}
double MidiProcessor::getOffset(double timeFromLastEventMS){ //  where 100BMP ??
	//std::cout <<"T:" << (double)MidiDeviceSingleton::Instance().ticks_per_quarter_note() << "  " << (double)MidiDeviceSingleton::Instance().tempo() << std::endl;
	return timeFromLastEventMS * (double)MidiDeviceSingleton::Instance().ticks_per_quarter_note() * (double)1000.0 / (double)MidiDeviceSingleton::Instance().tempo();
}
void MidiProcessor::play(midi::MidiEvent me, bool useOffset){
	MidiDeviceSingleton::Instance().play(me, useOffset);
	if (me.type() == midi::MidiEvent::META_SET_TEMPO){
		MidiDeviceSingleton::Instance().tempo(MidiDeviceSingleton::Instance().tempo());
		emit tempoChanged(MidiDeviceSingleton::Instance().tempo());
	}
}
void MidiProcessor::stop(){
	playing = 0;
	MidiDeviceSingleton::Instance().stop();
}
void MidiProcessor::play(midi::MidiTrack *mtLoaded_){
	if (mtLoaded_ != NULL){
		mtLoaded = mtLoaded_;
		playing = 1;
		playLoadedTrack();
	}
}
void MidiProcessor::playLoadedTrack(){
	size_t e = 0;
	size_t es = mtLoaded->size();
	while (playing) {
		if (e >= es || mtLoaded->events[e].type() == midi::MidiEvent::META_END_TRACK) {
			playing = false;
			emit trackEnd();
			e = 0;
			//emit to midiVisual that track end 
		}
		else {
			 MidiDeviceSingleton::Instance().play(mtLoaded->events[e], 1);
			++e;
		}	
		if (e % 10 == 0){
			qApp->processEvents();
		}
	}
	emit canDelete();
}
//_____
void MidiProcessor::setTPQN(size_t ticks_per_quater_note){
	 MidiDeviceSingleton::Instance().ticks_per_quarter_note(ticks_per_quater_note);
	 std::cout << "TPQN : " << MidiDeviceSingleton::Instance().ticks_per_quarter_note() <<std::endl;
}
void MidiProcessor::keyPressedNum(int num){
	midi::MidiEvent note_on;
	std::cout << " octave : " << octave <<  std::endl;
	int keyNum128 = num + (octave + 1) * 12;
	if (keyNum128 < 128){
		note_on = midi::MidiEvent::NoteOn((uint8_t)0, num + (octave + 1) * 12/*(uint8_t)midi::note_by_octave_key((uint8_t)(octave), (uint8_t)(num) )*/);
		std::cout << " key : " << (int)note_on.key() << std::endl;
		std::cout << "processP" << num << " octave : " << note_on.octave() << " num : " << (int)note_on.key() << std::endl;
		std::cout << "keyNum128 : " << keyNum128 << std::endl;
	}
	else{
		//std::cout << "keyNum128 > 128" << keyNum128 << std::endl;
		return;
	}
	MidiDeviceSingleton::Instance().play(note_on, 0);
	if (recording){
		timeOfThisEv = clock();
		note_on.offset((size_t)getOffset(timeOfThisEv - timeOfLastEv));
		emit drawRecNote(note_on, timeOfThisEv);
		std::cout << timeOfThisEv - timeOfLastEv << "  " << (size_t)getOffset(timeOfThisEv - timeOfLastEv) << std::endl;
		timeOfLastEv = timeOfThisEv;
		mtRec->events.push_back(note_on);
	}
}
void MidiProcessor::keyReleasedNum(int num){
	std::cout << "processR" << num << std::endl;
	midi::MidiEvent note_off;
	//note_off.data(1, fadeOut);
	if (num < 12){
		note_off = midi::MidiEvent::NoteOff((uint8_t)0, (uint8_t)midi::note_by_octave_key((uint8_t)octave, (uint8_t)num));
	}
	else{
		note_off = midi::MidiEvent::NoteOff((uint8_t)0, (uint8_t)midi::note_by_octave_key((uint8_t)octave + 1, (uint8_t)num-12));
	}
	MidiDeviceSingleton::Instance().play(note_off, 0);
	if (recording){
		timeOfThisEv = clock();
		note_off.offset(getOffset(timeOfThisEv - timeOfLastEv));
		timeOfLastEv = timeOfThisEv;
		mtRec->events.push_back(note_off);
	}
}
void MidiProcessor::instrumentChanged(int index){
	std::cout << "instruent" << index << std::endl;
	midi::MidiEvent instrument = midi::MidiEvent::Instrument((uint8_t)0, (uint8_t)index);
	MidiDeviceSingleton::Instance().play(instrument, 0);
	if (recording){
		timeOfThisEv = clock();
		instrument.offset(getOffset(timeOfThisEv - timeOfLastEv));
		timeOfLastEv = timeOfThisEv;
		mtRec->events.push_back(instrument);
	}
}
void MidiProcessor::octaveChanged(int index){
	octave = index;
	std::cout << "OCTAVE CHANGED :" << octave << std::endl;
}
void MidiProcessor::fadeInChanged(int index){
	fadeIn = index;
}
void MidiProcessor::fadeOutChanged(int index){
	fadeOut = index;
}
void MidiProcessor::stopAll(){
	 MidiDeviceSingleton::Instance().stop();
}
void MidiProcessor::percussionChanged(int percussion){
	this->percussion = percussion;
}
void MidiProcessor::percussionOn(){
	perc_on.data(0, percussion + 35);
	 MidiDeviceSingleton::Instance().play(perc_on, 0);
	std::cout << "precessionOn" << percussion << std::endl;
	if (recording){
		timeOfThisEv = clock();
		perc_on.offset(getOffset(timeOfThisEv - timeOfLastEv));
		timeOfLastEv = timeOfThisEv;
		mtRec->events.push_back(perc_on);
		emit drawRecPercussion(timeOfThisEv);
	}
}
void MidiProcessor::percussionOff(){
	perc_off.data(0, percussion + 35);
	 MidiDeviceSingleton::Instance().play(perc_off, 0);
	std::cout << "precessionOff" << percussion << std::endl;
	if (recording){
		timeOfThisEv = clock();
		perc_off.offset(getOffset(timeOfThisEv - timeOfLastEv));
		timeOfLastEv = timeOfThisEv;
		mtRec->events.push_back(perc_off);
	}
}
void MidiProcessor::startRecording(midi::MidiTrack *mtRec){
	this->mtRec = mtRec;
	timeOfLastEv = clock();
	recording = 1;
}
void MidiProcessor::stopRecording(){
	recording = 0;
}
MidiProcessor::~MidiProcessor()
{

}
