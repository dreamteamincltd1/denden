#include "MidiController.h"
#include "MidiPlayer.h"

MidiController::MidiController(MidiProcessor * processor)
{
	MidiPlayer *midiPlayer = new MidiPlayer(processor);
	midiPlayer->moveToThread(&playerThread);
	connect(&playerThread, &QThread::finished, midiPlayer, &QObject::deleteLater);
	connect(this, &MidiController::sendToPlay, midiPlayer, &MidiPlayer::play);
	connect(midiPlayer, &MidiPlayer::resultReady, this, &MidiController::handleResults);
	playerThread.start();
}
MidiController::~MidiController()
{
}
void MidiController::handleResults(){

}
void MidiController::play(midi::MidiEvent me){
	emit sendToPlay(me);
}