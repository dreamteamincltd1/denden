#pragma once
#include <qobject.h>
#include <midiprocessor.h>
#include <qthread.h>
class MidiController : public QObject
{
	Q_OBJECT
	QThread playerThread;
public:
	MidiController(MidiProcessor * processor);
	~MidiController();
	void play(midi::MidiEvent me);
	public slots:
		void handleResults();
	signals:
		void sendToPlay(midi::MidiEvent me);
};


