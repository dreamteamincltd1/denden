#ifndef MIDIKEY_H
#define MIDIKEY_H

#include <QPushButton>
#include <QObject>
class MidiKey : public QPushButton
{
	Q_OBJECT

public:
	MidiKey(QString *name, int num);
	~MidiKey();
	int getNum(){
		return num;
	}
signals:
	void pressed(int);
	void released(int);
public slots:
void pressed();
void released();

private:
	int num;
};

#endif // MIDIKEY_H
