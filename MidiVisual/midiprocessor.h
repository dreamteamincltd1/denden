#ifndef MIDIPROCESSOR_H
#define MIDIPROCESSOR_H

#include <QObject>
#include <QThread>

#include "..\midi\include\integers.h"
#include "..\midi\include\midi.h"
#include "..\midi\include\mididevice.h"
#include "..\midi\include\midievent.h"
#include "..\midi\include\midiheader.h"
#include "..\midi\include\miditrack.h"
#include "..\midi\include\rw_error.h"
#include "mididevicesingleton.h"
#include "time.h"

class MidiProcessor : public QObject
{
	Q_OBJECT
		friend class MidiVisual;
		friend class MidiPlayer;
public:
	MidiProcessor(QObject *parent);
	MidiProcessor();
	~MidiProcessor();
	double getOffset(double timeFromLastEventMS);
	public slots:
		void startRecording(midi::MidiTrack *mtRec);
		void stopRecording();
		void keyPressedNum(int num);
		void keyReleasedNum(int num);
		void instrumentChanged(int index);
		void octaveChanged(int index);
		void fadeInChanged(int index);
		void fadeOutChanged(int index);
		void stopAll();
		void percussionChanged(int percussion);
		void percussionOn();
		void percussionOff();
		void play(midi::MidiEvent me, bool useOffset);
		void play(midi::MidiTrack *mtLoaded_);
		void stop();
		void setTPQN(size_t ticks_per_quater_note);
	signals:
		void trackEnd();
		void canDelete();
		void octaveChanged();
		void tempoChanged(size_t tempo);
		void playOnDevice(midi::MidiEvent me);
		void drawRecNote(midi::MidiEvent me , time_t timeMS);
		void drawRecPercussion(time_t timeMS);
private:
	time_t timeOfLastEv , timeOfThisEv;
	int octave;
	int instrument;
	int percussion;
	int fadeIn, fadeOut;
	bool recording;
	bool playing;
	midi::MidiTrack *mtLoaded;
	void playLoadedTrack();
	midi::MidiTrack *mtRec;
	midi::MidiEvent perc_on;
	midi::MidiEvent perc_off;

};

#endif // MIDIPROCESSOR_H
