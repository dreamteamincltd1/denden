#ifndef MIDIVISUAL_H
#define MIDIVISUAL_H

#include <QtWidgets/QWidget>
#include <QPushButton>
#include <QGraphicsView>
#include <QGraphicsScene>
#include <QGraphicsRectItem>
#include "ui_midivisual.h"
#include  <Qstring>
#include  <QKeyEvent>
#include  <QObject>
#include  <QTimer>
#include  <QThread>
#include "virtualkeyboard.h"
#include "midiprocessor.h"
#include "midicontroller.h"
#include <QFileDialog>
#include <QScrollBar>
#include "..\\common\\keyboard.h"
#include <string>
#include <map>

#include "..\midi\include\integers.h"
#include "..\midi\include\midi.h"
#include "..\midi\include\mididevice.h"
#include "..\midi\include\midievent.h"
#include "..\midi\include\midiheader.h"
#include "..\midi\include\miditrack.h"
#include "..\midi\include\rw_error.h"

#include <atomic>
static std::atomic<bool> canDelete;

class MidiVisual : public QWidget
{
	Q_OBJECT

public:
	MidiVisual(QWidget *parent = 0);
	~MidiVisual();
private:
	QThread threadForPlaying,
		    threadForTP;
	QGraphicsScene *editorScene	;
	QGraphicsScene *percussionScene;
	Ui::MidiVisualClass ui;
	VirtualKeyboard *keyboard;
	MidiProcessor *trackPlayer;
	MidiProcessor *realTimePlayer;
	midi::Midi midi_data;
	KeyReader kr;
	midi::MidiTrack mtRec;
	midi::MidiTrack *mtLoaded;
	midi::Midi midi_merged;
	midi::Midi midi_rec;
	size_t es;
	int *keysX;
	int *keysW;
	QTimer *redrawTimer;
	//------------------------MS-------------Event-------------------------
	std::vector < std::pair< double, midi::MidiEvent > > eventsWithSeconds; // defult midiTrack with seconds
	std::vector < std::pair< double, midi::MidiEvent > > fidiTrack;// on all track : ticks - 100BMP , tempo  - 192 ticksPerQuaterNote  (fst event) , correct offsets 
	int percussionX;
	int percussionW;
	double lengthLines;
	int showMsToY;
	int octaveW;
	int ticks_per_second;
	double slidersY;
	double timeNow;
	double timeOfFstNote;
	int redrawTimerTicksMs;
	int playTimerTicksMs;
	int numOfLastEvent;
	bool fileLoaded;
	bool recCreated;
	bool playing ;
	bool showing;
	bool whantDelete;
	std::map<std::string, int> keysMap;
	size_t tempo_trackPlayer, tpqn_trackPlayer;

	void showFile();
	void keyPressEvent(QKeyEvent* pe);
	void keyReleaseEvent(QKeyEvent* re);
	void drawLines(int octave);
	void drawAllLines();
	void drawRect(int keyNum128, double globalOffset, QPen pen, QBrush brush);
	double getGlobalTimeMs_TrackPlayer(double lastEventTime, size_t offset);
	double getOffset_TrackPlayer(double timeFromLastEventMS);
	size_t getOffset100BPM_TrackPlayer(double eventTimeMs);
	public slots:
	void timerWhantMoveSLiders();
	void loadFile();
	void playFile();
	void closeFile();
	void deleteAll();
	void stopPlayFile();
	void newFile();
	void record();
	void stopRecord();
	void saveRecord();
	void deleteRecord();
	void octaveChangedSlider(int octave);
	void tempoChanged(size_t tempo);
	void drawRecordedNote(midi::MidiEvent me, time_t timeMS);
	void drawRecordedPercussion(time_t timeMS);
signals:
	void startRecordingTo(midi::MidiTrack *mRec);
	void keyPressedNumKeyboard(int num);
	void keyReleasedNumKeyboard(int num);
	void percussionOn();
	void percussionOff();
	void clearScenes();
	void playOnProcessor(midi::MidiEvent me, bool useOffset);
	void playThisTrackOnTrackPlayer(midi::MidiTrack *mtLoaded);
	void setTPQN(size_t ticks_per_quater_note);
	void stopPlayOnProcessor();
};

#endif // MIDIVISUAL_H
