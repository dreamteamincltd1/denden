#pragma once
#include <qobject.h>
#include <midiprocessor.h>

class MidiPlayer : public QObject
{
	Q_OBJECT
	//friend class MidiController;
public:
	MidiPlayer(MidiProcessor * processor);
	~MidiPlayer();
	public slots :
	void play(midi::MidiEvent me);
signals:
	void resultReady();

private:
	MidiProcessor *processor;
};
