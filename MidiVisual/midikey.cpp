#include "midikey.h"
#include <iostream>
MidiKey::MidiKey(QString *name , int num) : QPushButton()
{
	this -> setText(*name);
	this -> num = num;
}

void MidiKey::pressed(){ // if pressed sent signal with num of key
	emit pressed(this->num);
	std::cout << "P" << num << std::endl;
}
void MidiKey::released(){
	emit released(this->num);
	std::cout << "R" << num << std::endl;
}
MidiKey::~MidiKey()
{

}
