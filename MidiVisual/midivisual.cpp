#include "midivisual.h"
#include <iostream> 

size_t tempo100BPM = 60000000u / 100;
midi::MidiEvent tempoBMP100 = midi::MidiEvent::Tempo(tempo100BPM);
MidiVisual::MidiVisual(QWidget *parent)
	: QWidget(parent)
{
	ui.setupUi(this);
	keyboard = new VirtualKeyboard(ui.graphicsView, this);//keyboard
	ui.graphicsView->setScene(keyboard);
	///calc keys info 
	QSize sizeOfView = ui.editorView->size();
	keysX = new int[24];
	keysW = new int[24];
	int j = 0;
	int xW, wB = keyboard->pkeysB[0]->width(), h, wEditor = ui.editorView->size().width();
	octaveW = wEditor / 2 - keysW[1] / 2;
	percussionW = ui.percussionButton->width();
	for (int i = 0; i < 14; i++){ // get x of rects 
		xW = keyboard->pkeysW[i]->x();
		if (i == 0 || i == 3 || i == 7 || i == 10){ // no black
			keysX[j] = -wEditor / 2 + xW;
			++j;
		}
		else{// black
			keysX[j] = -wEditor / 2 + xW - wB / 3;
			++j;
			keysX[j] = -wEditor / 2 + xW + wB / 3;
			++j;
		}

	}
	for (int i = 0; i < 23; i++){ // get weigth of rects 
		keysW[i] = keysX[i + 1] - keysX[i];
	}
	keysW[23] = keysW[18];

	realTimePlayer = new MidiProcessor();// (this);
	trackPlayer = new MidiProcessor();
	connect(keyboard, SIGNAL(keyPressedNum(int)), realTimePlayer, SLOT(keyPressedNum(int)));
	connect(keyboard, SIGNAL(keyReleasedNum(int)), realTimePlayer, SLOT(keyReleasedNum(int)));
	connect(this, SIGNAL(keyPressedNumKeyboard(int)), keyboard, SIGNAL(keyPressedNum(int))); // connect to processor ??
	connect(this, SIGNAL(keyReleasedNumKeyboard(int)), keyboard, SIGNAL(keyReleasedNum(int)));
	connect(this, SIGNAL(percussionOn()), realTimePlayer, SLOT(percussionOn()));
	connect(this, SIGNAL(percussionOff()), realTimePlayer, SLOT(percussionOff()));
	connect(ui.families, SIGNAL(activated(int)), ui.instruments, SLOT(familyChanged(int)));//connect combo boxes
	connect(ui.instruments, SIGNAL(instrumentChanged(int)), realTimePlayer, SLOT(instrumentChanged(int)));
	connect(ui.octave, SIGNAL(valueChanged(int)), realTimePlayer, SLOT(octaveChanged(int)));//connect sliders
	connect(ui.octave, SIGNAL(valueChanged(int)), this, SLOT(octaveChangedSlider(int)));//connect sliders
	connect(ui.fadeIn, SIGNAL(valueChanged(int)), realTimePlayer, SLOT(fadeInChanged(int)));
	connect(ui.fadeIn, SIGNAL(valueChanged(int)), realTimePlayer, SLOT(fadeInChanged(int)));
	connect(ui.fadeIn, SIGNAL(valueChanged(int)), realTimePlayer, SLOT(fadeInChanged(int)));
	connect(ui.stopButton, SIGNAL(pressed()), realTimePlayer, SLOT(stopAll()));
	connect(ui.percussion, SIGNAL(activated(int)), realTimePlayer, SLOT(percussionChanged(int)));//connect combo boxes
	connect(ui.percussionButton, SIGNAL(pressed()), realTimePlayer, SLOT(percussionOn()));
	connect(ui.percussionButton, SIGNAL(released()), realTimePlayer, SLOT(percussionOff()));
	connect(this, SIGNAL(setTPQN(size_t)), realTimePlayer, SLOT(setTPQN(size_t)));
	QStringList families;//families/intsruments 
	families << "Piano" <<
		"Chromatic percussion" <<
		"Organ" <<
		"Guitar" <<
		"Bass" <<
		"Strings" <<
		"Ensemble" <<
		"Brass" <<
		"Reed" <<
		"Pipe" <<
		"Synth lead" <<
		"Synth pad" <<
		"Synth effects" <<
		"Ethnic" <<
		"Percussive" <<
		"Sound effects";
	ui.families->addItems(families);
	ui.families->show();
	QStringList percussion;
	percussion << "Acoustic Bass Drum" <<
		"Bass Drum 1" <<
		"Side Stick" <<
		"Acoustic Snare" <<
		"Hand Clap" <<
		"Electric Snare" <<
		"Low Floor Tom" <<
		"Closed Hi-Hat" <<
		"High Floor Tom" <<
		"Pedal Hi-Hat" <<
		"Low Tom" <<
		"Open Hi-Hat" <<
		"Low-Mid Tom" <<
		"Hi-Mid Tom" <<
		"Crash Cymbal 1" <<
		"High Tom" <<
		"Ride Cymbal 1" <<
		"Chinese Cymbal" <<
		"Ride Bell" <<
		"Tambourine" <<
		"Splash Cymbal" <<
		"Cowbell" <<
		"Crash Cymbal 2" <<
		"Vibraslap" <<
		"Ride Cymbal 2" <<
		"Hi Bongo" <<
		"Low Bongo" <<
		"Mute Hi Conga" <<
		"Open Hi Conga" <<
		"Low Conga" <<
		"High Timbale" <<
		"Low Timbale" <<
		"High Agogo" <<
		"Low Agogo" <<
		"Cabasa" <<
		"Maracas" <<
		"Short Whistle" <<
		"Long Whistle" <<
		"Short Guiro" <<
		"Long Guiro" <<
		"Claves" <<
		"Hi Wood Block" <<
		"Low Wood Block" <<
		"Mute Cuica" <<
		"Open Cuica" <<
		"Mute Triangle" <<
		"Open Triangle";
	ui.percussion->addItems(percussion);//percussion
	ui.percussion->show();
	//connect buttons ( play , load , stop)
	connect(ui.loadButton, SIGNAL(pressed()), this, SLOT(loadFile()));
	connect(ui.playButton, SIGNAL(pressed()), this, SLOT(playFile()));
	connect(ui.stopButton, SIGNAL(pressed()), this, SLOT(stopPlayFile()));
	connect(ui.closeButton, SIGNAL(pressed()), this, SLOT(closeFile()));
	//rec buttons
	connect(ui.newButton, SIGNAL(pressed()), this, SLOT(newFile()));
	connect(ui.recordButton, SIGNAL(pressed()), this, SLOT(record()));
	connect(ui.stopRecButton, SIGNAL(pressed()), this, SLOT(stopRecord()));
	connect(ui.saveRecButton, SIGNAL(pressed()), this, SLOT(saveRecord()));
	connect(ui.delRecButton, SIGNAL(pressed()), this, SLOT(deleteRecord()));
	connect(this, SIGNAL(startRecordingTo(midi::MidiTrack *)), realTimePlayer, SLOT(startRecording(midi::MidiTrack*)));
	connect(ui.stopRecButton, SIGNAL(pressed()), realTimePlayer, SLOT(stopRecording()));// (this);
	connect(this, SIGNAL(playThisTrackOnTrackPlayer(midi::MidiTrack*)), trackPlayer, SLOT(play(midi::MidiTrack*)));
	connect(ui.stopButton, SIGNAL(pressed()), trackPlayer, SLOT(stop()));
	connect(this, SIGNAL(playOnProcessor(midi::MidiEvent, bool)), trackPlayer, SLOT(play(midi::MidiEvent, bool)));
	connect(trackPlayer, SIGNAL(trackEnd()), this, SLOT(stopPlayFile()));
	connect(trackPlayer, SIGNAL(canDelete()), this, SLOT(deleteAll()));
	connect(this, SIGNAL(stopPlayOnProcessor()), trackPlayer, SLOT(stop()));
	connect(this, SIGNAL(setTPQN(size_t)), trackPlayer, SLOT(setTPQN(size_t)));
	connect(trackPlayer, SIGNAL(tempoChanged(size_t)), this, SLOT(tempoChanged(size_t)));
	//rec drawind
	connect(realTimePlayer, SIGNAL(drawRecNote(midi::MidiEvent, time_t)), this, SLOT(drawRecordedNote(midi::MidiEvent, time_t)));
	connect(realTimePlayer, SIGNAL(drawRecPercussion(time_t)), this, SLOT(drawRecordedPercussion(time_t)));
	//connect scenes cleaner


	//
	editorScene = new QGraphicsScene();
	percussionScene = new QGraphicsScene();
	connect(this, SIGNAL( clearScenes() ), this->percussionScene, SLOT( clear() )  );
	connect(this, SIGNAL(clearScenes()), this->editorScene, SLOT(clear()));
	redrawTimer = new QTimer();
	connect(redrawTimer, SIGNAL(timeout()), this, SLOT(timerWhantMoveSLiders()));
	fileLoaded = 0;
	recCreated = 0;
	ticks_per_second = 0;
	slidersY = 0;
	redrawTimerTicksMs = 30; 
	showMsToY = 5;
	playing = 0;

	//move trackPlayer and MidiDeviceSingleton to dif threads
	threadForPlaying.start(QThread::HighestPriority);
	threadForTP.start(QThread::HighPriority);
	trackPlayer->moveToThread(&threadForTP);
	MidiDeviceSingleton::Instance().moveToThread(&threadForPlaying);
	whantDelete = 0;

	//if (MidiDeviceSingleton::Instance().thread() == this->thread()
	//	|| MidiDeviceSingleton::Instance().thread() == trackPlayer->thread()
	//	|| MidiDeviceSingleton::Instance().thread() == realTimePlayer->thread()){
	//	std::cout << "1 no , it's not in dif thread" << std::endl;
	//}
	//MidiDeviceSingleton::Instance().moveToThread(&threadForPlaying);
	//if (MidiDeviceSingleton::Instance().thread() == this->thread()
	//	|| MidiDeviceSingleton::Instance().thread() == trackPlayer->thread()
	//	|| MidiDeviceSingleton::Instance().thread() == realTimePlayer->thread()){
	//	std::cout << "2 no , it's not in dif thread" << std:: endl;
	//}
}
void MidiVisual::drawRecordedPercussion(time_t timeMS){
	QPen pen;  // settings for rects
	QBrush brush(Qt::red);
	pen.setStyle(Qt::SolidLine);
	pen.setWidth(3);
	pen.setBrush(Qt::darkGray);
	pen.setCapStyle(Qt::RoundCap);
	pen.setJoinStyle(Qt::RoundJoin);
	percussionScene->addRect(-ui.percussionButton->width() / 2, (size_t)(/*timeMS / showMsToY*/slidersY), ui.percussionButton->width(), 20, pen, brush);
}
void MidiVisual::drawRecordedNote(midi::MidiEvent me, time_t timeMS){
	QPen pen;  // settings for rects
	QBrush brush(Qt::red);
	pen.setStyle(Qt::SolidLine);
	pen.setWidth(3);
	pen.setBrush(Qt::darkGray);
	pen.setCapStyle(Qt::RoundCap);
	pen.setJoinStyle(Qt::RoundJoin);
	//int keyNum = 1;// me.  //keysMap[me.note_name()];
	//std::cout << "me.octave() : " << me.octave() << " keyNum : " << keyNum << std::endl;
	drawRect((int)me.key(), (size_t)(/*timeMS / showMsToY*/slidersY), pen, brush);
}
double MidiVisual::getGlobalTimeMs_TrackPlayer(double lastEventTime, size_t offset){
	return lastEventTime + (offset * MidiDeviceSingleton::Instance().tempo()) / (MidiDeviceSingleton::Instance().ticks_per_quarter_note() * 1000);
	//�� = (������ * ����) / (���� * 1000)
}
void MidiVisual::tempoChanged(size_t tempo){
	tempo_trackPlayer = tempo;
}
double MidiVisual::getOffset_TrackPlayer(double timeFromLastEventMS){ //  where 100BMP ??
	return (timeFromLastEventMS * (tpqn_trackPlayer * 1000) / tempo_trackPlayer);
}
size_t MidiVisual::getOffset100BPM_TrackPlayer(double eventTimeMs){
	return eventTimeMs * tpqn_trackPlayer * 1000 / tempo100BPM;

}
void MidiVisual::timerWhantMoveSLiders(){
//	if (playing > 0){
		slidersY += (double)redrawTimerTicksMs / (double)showMsToY; //redrawTimerTicksMs / showMsToY;
		ui.editorView->verticalScrollBar()->setValue(slidersY);
		ui.percussionView->verticalScrollBar()->setValue(slidersY);
//	}
}
void MidiVisual::octaveChangedSlider(int octave){
	ui.editorView->horizontalScrollBar()->setValue(octave * octaveW);
}
void MidiVisual::stopPlayFile(){
	emit stopPlayOnProcessor();
	redrawTimer->stop();
	MidiDeviceSingleton::Instance().stop();
	slidersY = 0;
	ui.editorView->verticalScrollBar()->setValue(slidersY);
	ui.percussionView->verticalScrollBar()->setValue(slidersY);
}
void MidiVisual::newFile(){
	if (recCreated){
		deleteRecord();
	}
	if (fileLoaded){
		closeFile();
	}
	MidiDeviceSingleton::Instance().tempo(midi::bpm2tempo(120));
	MidiDeviceSingleton::Instance().ticks_per_quarter_note(96);
	mtRec.events.push_back(midi::MidiEvent::Tempo(MidiDeviceSingleton::Instance().tempo() ) );
	mtRec.events[0].offset(0);
	//ask length of new track ( now 5 minute defult )
	lengthLines = 1000 * 60 * 5 / showMsToY;
	drawAllLines();
	editorScene->setSceneRect(QRectF(-octaveW, 0, keysX[23] + (12) * octaveW + keysW[21] + keysW[22] / 2, lengthLines));
	percussionScene->setSceneRect(QRectF(-ui.percussionView->width() / 2, 0, ui.percussionView->width(), lengthLines));
	//add and show scenes
	ui.percussionView->setScene(percussionScene);
	ui.editorView->setScene(editorScene);
	ui.editorView->show();
	ui.percussionView->show();
	recCreated = 1;
	//showing = 0;
}
void MidiVisual::record(){
	if (fileLoaded || recCreated){
		slidersY = 0;
		emit startRecordingTo(&mtRec);
		redrawTimer->start(redrawTimerTicksMs);
	}
	//realTimePlayer start recording to mtRec
}
void MidiVisual::stopRecord(){
	redrawTimer->stop();
	mtRec.events.push_back(midi::MidiEvent::EndOfTrack());
	mtRec.events[mtRec.events.size() - 1].offset(0);
	//processor stop recording to mtRec
}
void MidiVisual::saveRecord(){
	midi::MidiHeader mh = midi::MidiHeader::SingleTrack(MidiDeviceSingleton::Instance().ticks_per_quarter_note());
	midi_rec.setHeader( mh );
	midi_rec.tracks.push_back(mtRec);
	if (fileLoaded){
		midi_rec.tracks.push_back(*mtLoaded);
		midi_rec = midi_rec.merge();
	}
	QString qFileName = QFileDialog::getSaveFileName(this, tr("Save File"), "/path/to/file/", tr("Midi Files (*.mid)"));
	std::string fileName = qFileName.toStdString();
	const char* file_midi = fileName.c_str();

	size_t file_size;
	FILE* midi_out = fopen(file_midi, "wb");
	if (!midi_out) {
		fprintf(stderr, "Unable to open output MIDI file\n");
		exit(-2);
	}

	// 4 MB should be enough for testing
	uint8_t* buf = new uint8_t[0x10000000];
	try {
		file_size = midi_rec.write(buf);
	}
	catch (...) {
		fprintf(stderr, "Oops! Seems like you have a very big file=)\n");
		exit(-3);
	}

	fwrite(buf, 1, file_size, midi_out);
	fclose(midi_out);
	delete[] buf;
}
void MidiVisual::deleteRecord(){
	closeFile();
}
void MidiVisual::playFile(){
	if (!fileLoaded && mtLoaded != NULL && mtLoaded->size() > 0){
		return;
	}
	playing = 1;
	std::cout << "TIMER START !!" << std::endl;
	redrawTimer->start(redrawTimerTicksMs);
	emit playThisTrackOnTrackPlayer(mtLoaded);
}
void MidiVisual::drawLines(int octave){
	int xW, wEditor = ui.editorView->size().width(), j = 0;
	octaveW = wEditor / 2 - keysW[1] / 2;
	for (int i = 0; i < 14; i++){//add keys lines
		xW = keyboard->pkeysW[i]->x();
		if (i == 0 || i == 3 || i == 7 || i == 10){ // no black
			editorScene->addLine(QLineF(keysX[j] + (octave + 1) * octaveW, -wEditor / 2, keysX[j] + (octave + 1) * octaveW, lengthLines + 100), QPen(Qt::black, 2));
			++j;
		}
		else{ // black
			editorScene->addLine(QLineF(keysX[j] + (octave + 1) * octaveW, -wEditor / 2, keysX[j] + (octave + 1) * octaveW, lengthLines + 100), QPen(Qt::black, 2)) + (octave + 1) * octaveW;
			++j;
			editorScene->addLine(QLineF(keysX[j] + (octave + 1) * octaveW, -wEditor / 2, keysX[j] + (octave + 1) * octaveW, lengthLines + 100), QPen(Qt::black, 2)) + (octave + 1) * octaveW;
			++j;
		}
		std::cout << " j " << j << std::endl;
	}
	if (octave == 11){ 
		editorScene->addLine(QLineF(keysX[23] + (octave + 1) * octaveW + keysW[21] + keysW[22] / 2, -wEditor / 2, keysX[23] + (octave + 1) * octaveW + keysW[21] + keysW[22] / 2, lengthLines + 100), QPen(Qt::black, 2));
	}
}
void MidiVisual::drawAllLines(){
	for (int i = -1; i <= 12;){
		std::cout << i << std::endl;
		drawLines(i);
		++i;
		++i;
	}
}
void MidiVisual::drawRect(int keyNum128, double y, QPen pen , QBrush brush){
	int octave = (keyNum128 - 1) / 12;
	int keyNum12 = ( keyNum128 - 1 ) % 12;
	std::cout << "oct :" << octave << "key12 : " << keyNum12 << "key128 : " << keyNum128 << std::endl;
//	if (octave % 2 == 0){
		editorScene->addRect(keysX[keyNum12] + (octave)* octaveW, y, keysW[keyNum12], 20, pen, brush);
	//}
	//else{
	//	editorScene->addRect(keysX[keyNum12 + 12] + (octave)* octaveW, y, keysW[keyNum12], 20, pen, brush);
	//}
}
void MidiVisual::closeFile(){
	whantDelete = 1;
	if (playing){
		stopPlayFile();//after stop emmit deleteAll();
	}
	else{
		deleteAll();
	}
	/*else{
		if (showing)
	}*/

}
void MidiVisual::deleteAll(){
	if (whantDelete){
		whantDelete = 0;
		if (fileLoaded){
			fileLoaded = 0;
			QList<QGraphicsItem *> topLevels;
			//clearScenes
			foreach(QGraphicsItem  *item, editorScene->items()) {
				if (!item->parentItem())
					topLevels << item;
			}
			foreach(QGraphicsItem  *item, percussionScene->items()) {
				if (!item->parentItem())
					topLevels << item;
			}
			qDeleteAll(topLevels);
			midi_data.tracks.clear();
			mtLoaded->events.clear();
			eventsWithSeconds.clear();
			editorScene->setSceneRect(QRectF(0, 0, ui.editorView->width(), ui.editorView->height()));
			percussionScene->setSceneRect(QRectF(0, 0, ui.percussionView->width(), ui.percussionView->height()));
		}
		if (recCreated){
			recCreated = 0;
			QList<QGraphicsItem *> topLevels;
			//clearScenes
			foreach(QGraphicsItem  *item, editorScene->items()) {
				if (!item->parentItem())
					topLevels << item;
			}
			foreach(QGraphicsItem  *item, percussionScene->items()) {
				if (!item->parentItem())
					topLevels << item;
			}
			qDeleteAll(topLevels);
			mtRec.events.clear();
			midi_rec.tracks.clear();
			slidersY = 0;
			ui.editorView->verticalScrollBar()->setValue(slidersY);
			ui.percussionView->verticalScrollBar()->setValue(slidersY);
		}
	}
}
void MidiVisual::showFile(){
	canDelete = 0;
	lengthLines = eventsWithSeconds[eventsWithSeconds.size() - 1].first / showMsToY;
	int wEditor = ui.editorView->size().width();
	percussionScene->addLine(QLineF(-ui.percussionButton->width() / 2, -wEditor / 2, -ui.percussionButton->width() / 2, lengthLines + 100), QPen(Qt::black, 2));//add percussion line
	percussionScene->addLine(QLineF(+ui.percussionButton->width() / 2, -wEditor / 2, +ui.percussionButton->width() / 2, lengthLines + 100), QPen(Qt::black, 2));//add percussion line
	drawAllLines();
	editorScene->setSceneRect(QRectF(-octaveW, 0, keysX[23] + (12) * octaveW + keysW[21] + keysW[22] / 2, lengthLines));
	percussionScene->setSceneRect(QRectF(-ui.percussionView->width() / 2, 0, ui.percussionView->width(), lengthLines));
	ui.percussionView->setScene(percussionScene);
	ui.editorView->setScene(editorScene);
	ui.percussionView->show();
	ui.editorView->show();
	//emit clearScenes();
	QPen pen;  // settings for rects
	QBrush brush(Qt::black);
	pen.setStyle(Qt::SolidLine);
	pen.setWidth(3);
	pen.setBrush(Qt::darkGray);
	pen.setCapStyle(Qt::RoundCap);
	pen.setJoinStyle(Qt::RoundJoin);
	size_t e = 0;
	double globalOffset = 0;
	int keyNum;
	//std::map<std::string, int> keysMap;
	const char* notes[] = {
		"C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B"
	};
	std::map<std::string, int>::iterator it = keysMap.begin();
	for (int i = 0; i < 12; ++i){//create map for notes - keys conection
		keysMap.insert(it, std::pair<const char*, int>(notes[i], i));
	}

	std::cout << " NOTE 0 : " << notes[0] << std::endl;
	while (!(e >= es || eventsWithSeconds[e].second.type() == midi::MidiEvent::META_END_TRACK)){
		globalOffset += (double)mtLoaded->events[e].offset();
		if (strcmp(  eventsWithSeconds[e].second.note_name(), "<Not a note>"   ) != 0){ // isNote
			if (eventsWithSeconds[e].second.channel() == 9){ //percussion
				if (strcmp(eventsWithSeconds[e].second.note_name(), "<Not a percussion>") != 0){
					percussionScene->addRect(-ui.percussionButton->width() / 2, (size_t)(eventsWithSeconds[e].first / showMsToY), ui.percussionButton->width(), 20, pen, brush);
				}
			}
			else{
				if (eventsWithSeconds[e].second.type() == midi::MidiEvent::MSG_NOTE_ON){
					//keyNum = keysMap[mtLoaded->events[e].note_name()];
					drawRect( (int)mtLoaded->events[e].key(), (size_t)(eventsWithSeconds[e].first / showMsToY), pen, brush);
					//std::cout << "NOTEON : key - " << keyNum << " octave - " << eventsWithSeconds[e].second.octave() << "ev octave - " << mtLoaded->events[e].octave() << " time - " << (size_t)(eventsWithSeconds[e].first / showMsToY) << std::endl;
				}
				else{
					//std::cout << "note Off : ";
				}
			}
		}
		else{//Not a note
		}
		e++;
	}
	canDelete = 1;
}
void MidiVisual::loadFile(){
	QStringList fileNames = QFileDialog::getOpenFileNames(this, tr("Open File"), "/path/to/file/", tr("Midi Files (*.mid)"));
	if (fileLoaded || recCreated){
		fileLoaded = 0;
		this->closeFile();
	}
	
	if (fileNames.size() != 0){ // loads last chosen track  
		QString file_midiQ = fileNames[0];
		std::cout << file_midiQ.toStdString() << std::endl;
		std::string fileName = file_midiQ.toStdString();
		const char* file_midi = fileName.c_str();
		FILE* midi_file = fopen(file_midi, "rb");
		if (!midi_file) {
			fprintf(stderr, "Unable to open MIDI file %s\n", file_midi);
			exit(-2);
		}
		fseek(midi_file, 0, SEEK_END);
		size_t file_size = ftell(midi_file);
		rewind(midi_file);
		uint8_t* buf = new uint8_t[file_size];
		fread(buf, 1, file_size, midi_file);
		fclose(midi_file);
		try {
			midi_data.read(buf);
		}
		catch (rw_error e) {
			uint8_t* where = e.where();
			fileLoaded = 0;
			fprintf(stderr, "MIDI file reading error: %s at pos %u\n"
				"> %02x %02x %02x %02x ..\n", e.what(), where - buf,
				(unsigned)where[0], (unsigned)where[1], (unsigned)where[2], (unsigned)where[3]
				);
			exit(-3);
		}
		catch (std::exception e) {
			fileLoaded = 0;
			fprintf(stderr, "MIDI file error: %s\n", e.what());
			exit(-3);
		}
		delete[] buf;

		fileLoaded = 1;
		midi_merged = midi_data.merge();
		bool hq = 0;

		MidiDeviceSingleton::Instance().ticks_per_quarter_note( midi_merged.ticks_per_quarter_note() );

		std::cout << "loaded : midi_merged.ticks_per_quarter_note() : " << midi_data.ticks_per_quarter_note() << std::endl;

		std::cout << "merged : midi_merged.ticks_per_quarter_note() : " << midi_merged.ticks_per_quarter_note() << std::endl;

		mtLoaded = new midi::MidiTrack();
		*mtLoaded = midi_merged.tracks[0];
		es = mtLoaded->events.size();
		
		//get time in ms for each event
		size_t e = 0;
		double lastTimeMs = 0, timeOfEventMs = 0;
		while (!(e >= es || mtLoaded->events[e].type() == midi::MidiEvent::META_END_TRACK)){
			timeOfEventMs = getGlobalTimeMs_TrackPlayer(lastTimeMs, mtLoaded->events[e].offset());
			eventsWithSeconds.push_back(std::pair<double, midi::MidiEvent>(timeOfEventMs, mtLoaded->events[e]));
			lastTimeMs = timeOfEventMs;
			if (mtLoaded->events[e].type() == midi::MidiEvent::META_SET_TEMPO){
				MidiDeviceSingleton::Instance().play(mtLoaded->events[e], 0);
				Sleep(10);
			}
			e++;
		}
		eventsWithSeconds.push_back(std::pair<double, midi::MidiEvent>(timeOfEventMs + 500, midi::MidiEvent::EndOfTrack()));//add end of trac
		showFile();
	}
}

void MidiVisual::keyPressEvent(QKeyEvent* pe)
{
	int keysNumB[10] { 2, 4, 7, 9, 11, 14, 16, 19, 21, 23 };
	int keysNumW[14] { 1, 3, 5, 6, 8, 10, 12, 13, 15, 17, 18, 20, 22, 24 };
	//keysNamesB = new QString("234567ghkl;");
	switch (pe->key()) {
	case Qt::Key_2://---------------black----------------------
		emit keyPressedNumKeyboard(keysNumB[0]);
		break;
	case Qt::Key_3:
		emit keyPressedNumKeyboard(keysNumB[1]);
		break;
	case Qt::Key_4:
		emit keyPressedNumKeyboard(keysNumB[2]);
		break;
	case Qt::Key_5:
		emit keyPressedNumKeyboard(keysNumB[3]);
		break;
	case Qt::Key_6:
		emit keyPressedNumKeyboard(keysNumB[4]);
		break;
	case Qt::Key_7:
		emit keyPressedNumKeyboard(keysNumB[5]);
		break;
	case Qt::Key_G:
		emit keyPressedNumKeyboard(keysNumB[6]);
		break;
	case Qt::Key_H:
		emit keyPressedNumKeyboard(keysNumB[7]);
		break;
	case Qt::Key_K:
		emit keyPressedNumKeyboard(keysNumB[8]);
		break;
	case Qt::Key_L:
		emit keyPressedNumKeyboard(keysNumB[9]);
		break;
	//keysNamesW = new QString("QWERTYUVBNM,./");
	case Qt::Key_Q://---------------white----------------------
		emit keyPressedNumKeyboard(keysNumW[0]);
		break;
	case Qt::Key_W:
		emit keyPressedNumKeyboard(keysNumW[1]);
		break;
	case Qt::Key_E:
		emit keyPressedNumKeyboard(keysNumW[2]);
		break;
	case Qt::Key_R:
		emit keyPressedNumKeyboard(keysNumW[3]);
		break;
	case Qt::Key_T:
		emit keyPressedNumKeyboard(keysNumW[4]);
		break;
	case Qt::Key_Y:
		emit keyPressedNumKeyboard(keysNumW[5]);
		break;
	case Qt::Key_U:
		emit keyPressedNumKeyboard(keysNumW[6]);
		break;
	case Qt::Key_V:
		emit keyPressedNumKeyboard(keysNumW[7]);
		break;
	case Qt::Key_B:
		emit keyPressedNumKeyboard(keysNumW[8]);
		break;
	case Qt::Key_N:
		emit keyPressedNumKeyboard(keysNumW[9]);
		break;
	case Qt::Key_M:
		emit keyPressedNumKeyboard(keysNumW[10]);
		break;
	case 44 : // ','
		emit keyPressedNumKeyboard(keysNumW[11]);
		break;
	case 46 : // '.' 
		emit keyPressedNumKeyboard(keysNumW[12]);
		break;
	case 47: // '/'
		emit keyPressedNumKeyboard(keysNumW[13]);
		break;
	case Qt::Key_Alt: 
		emit percussionOn();
		break;
	default:
		std::cout <<"you pressed key:"<< pe->key() << std::endl;
		QWidget::keyPressEvent(pe);

	}
}
void MidiVisual::keyReleaseEvent(QKeyEvent* re)
{
	int keysNumB[10] { 2, 4, 7, 9, 11, 14, 16, 19, 21, 23 };
	int keysNumW[14] { 1, 3, 5, 6, 8, 10, 12, 13, 15, 17, 18, 20, 22, 24 };
	//keysNamesB = new QString("234567ghkl;");
	switch (re->key()) {
	case Qt::Key_2://---------------black----------------------
		emit keyReleasedNumKeyboard(keysNumB[0]);
		break;
	case Qt::Key_3:
		emit keyReleasedNumKeyboard(keysNumB[1]);
		break;
	case Qt::Key_4:
		emit keyReleasedNumKeyboard(keysNumB[2]);
		break;
	case Qt::Key_5:
		emit keyReleasedNumKeyboard(keysNumB[3]);
		break;
	case Qt::Key_6:
		emit keyReleasedNumKeyboard(keysNumB[4]);
		break;
	case Qt::Key_7:
		emit keyReleasedNumKeyboard(keysNumB[5]);
		break;
	case Qt::Key_G:
		emit keyReleasedNumKeyboard(keysNumB[6]);
		break;
	case Qt::Key_H:
		emit keyReleasedNumKeyboard(keysNumB[7]);
		break;
	case Qt::Key_K:
		emit keyReleasedNumKeyboard(keysNumB[8]);
		break;
	case Qt::Key_L:
		emit keyReleasedNumKeyboard(keysNumB[9]);
		break;
		//keysNamesW = new QString("QWERTYUVBNM,./");
	case Qt::Key_Q://---------------white----------------------
		emit keyReleasedNumKeyboard(keysNumW[0]);
		break;
	case Qt::Key_W:
		emit keyReleasedNumKeyboard(keysNumW[1]);
		break;
	case Qt::Key_E:
		emit keyReleasedNumKeyboard(keysNumW[2]);
		break;
	case Qt::Key_R:
		emit keyReleasedNumKeyboard(keysNumW[3]);
		break;
	case Qt::Key_T:
		emit keyReleasedNumKeyboard(keysNumW[4]);
		break;
	case Qt::Key_Y:
		emit keyReleasedNumKeyboard(keysNumW[5]);
		break;
	case Qt::Key_U:
		emit keyReleasedNumKeyboard(keysNumW[6]);
		break;
	case Qt::Key_V:
		emit keyReleasedNumKeyboard(keysNumW[7]);
		break;
	case Qt::Key_B:
		emit keyReleasedNumKeyboard(keysNumW[8]);
		break;
	case Qt::Key_N:
		emit keyReleasedNumKeyboard(keysNumW[9]);
		break;
	case Qt::Key_M:
		emit keyReleasedNumKeyboard(keysNumW[10]);
		break;
	case 44: // ','
		emit keyReleasedNumKeyboard(keysNumW[11]);
		break;
	case 46: // '.' 
		emit keyReleasedNumKeyboard(keysNumW[12]);
		break;
	case 47: // '/'
		emit keyReleasedNumKeyboard(keysNumW[13]);
		break;
	case Qt::Key_Alt:
		emit percussionOff();
		break;
	default:
		std::cout << "you released key:" << re->key() << std::endl;
		QWidget::keyReleaseEvent(re);

	}
}
MidiVisual::~MidiVisual()
{
	redrawTimer->stop();
	closeFile();
	threadForPlaying.exit();
	threadForTP.exit();
}
