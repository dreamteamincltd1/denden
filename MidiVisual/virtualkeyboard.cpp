#include "virtualkeyboard.h"

VirtualKeyboard::VirtualKeyboard(QGraphicsView *view, QObject *parent)
	: QGraphicsScene(parent)
{
	QSize sizeOfView = view->size();
	QSize sizeW(sizeOfView.width() / 14, sizeOfView.height() - 5);
	QSize sizeB(sizeOfView.width() / 28, sizeOfView.height() / 1.7);
	
	keysNamesB = new QString("234567ghkl;");
	keysNumB = new int[10] { 2, 4, 7, 9, 11, 14, 16, 19, 21, 23 };
	keysNamesW = new QString("QWERTYUVBNM,./");
	keysNumW = new int[14] { 1, 3, 5, 6, 8, 10, 12, 13, 15,17,18,20,22,24 };
	for (int i = 0; i < 14; i++){ // add white keys 
		pkeysW[i] = new MidiKey(&(QString)(*keysNamesW)[i], keysNumW[i] );
		pkeysW[i]->setFixedSize(sizeW);
		pkeysW[i]->setStyleSheet("background-color: rgb(255,255,255);");
		pkeysW[i]->setGeometry(QRect((sizeOfView.width() / 14)*(i), 0, sizeW.width(), sizeW.height()));
		this->addWidget(pkeysW[i]);
		connect(pkeysW[i], SIGNAL( pressed() ), pkeysW[i], SLOT( pressed() ) );
		connect(pkeysW[i], SIGNAL(released()), pkeysW[i], SLOT(released()));
		connect( pkeysW[i], SIGNAL( pressed(int) ), this, SIGNAL(keyPressedNum(int)) );
		connect(pkeysW[i], SIGNAL(released(int)), this, SIGNAL(keyReleasedNum(int)));
	}
	int shift = 0;
	for (int i = 0; i < 10; i++){ // add black keys 
		pkeysB[i] = new MidiKey(&(QString)(*keysNamesB)[i], keysNumB[i] );
		pkeysB[i]->setFixedSize(sizeB);
		pkeysB[i]->setStyleSheet("background-color: rgb(20,20,20);");
		if (i == 2 || i == 5 || i == 7){
			++shift;
		}
		pkeysB[i]->setGeometry(QRect((sizeOfView.width() / 14)*(i + shift + 0.75), 0, sizeB.width(), sizeB.height()));
		this->addWidget(pkeysB[i]);
		connect(pkeysB[i], SIGNAL(pressed()), pkeysB[i], SLOT(pressed())); //catch signals from pressed/released keys 
		connect(pkeysB[i], SIGNAL(released()), pkeysB[i], SLOT(released()));
		connect(pkeysB[i], SIGNAL(pressed(int)), this, SIGNAL(keyPressedNum(int))); //resend signals from pressed/released keys with num key 
		connect(pkeysB[i], SIGNAL(released(int)), this, SIGNAL(keyReleasedNum(int)));
	}
}
VirtualKeyboard::~VirtualKeyboard()
{

}
