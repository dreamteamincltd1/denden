#include "instrumentsbox.h"
#include <iostream>
void InstrumentsBox::showInstrumentsFromeFamily(){
	this->clear();
	instrInSelectedFamily->clear();
	for (int i = familyNum * 8; i < familyNum * 8 + 8; ++i){
		(*instrInSelectedFamily) << instruments[i];
	}
	this->addItems(*instrInSelectedFamily);
	this->show();
}
InstrumentsBox::InstrumentsBox(QWidget *parent)
	: QComboBox(parent )
{
	instrInSelectedFamily = new QStringList;
	instruments = new QString[128]{
		"Acoustic Grand",
			"Bright Acoustic",
			"Electric Grand",
			"Honky-Tonk",
			"Electric Piano 1 (Rhodes piano)",
			"Electric Piano 2 (Chorused piano)",
			"Harpsichord",
			"Clav",
			"Celesta",
			"Glockenspiel",
			"Music Box",
			"Vibraphone",
			"Marimba",
			"Xylophone",
			"Tubular Bells",
			"Dulcimer (Santur)",
			"Drawbar Organ (Hammond)",
			"Percussive Organ",
			"Rock Organ",
			"Church Organ",
			"Reed Organ",
			"Accoridan (French)",
			"Harmonica",
			"Tango Accordian (Band neon)",
			"Acoustic Guitar (nylon)",
			"Acoustic Guitar (steel)",
			"Electric Guitar (jazz)",
			"Electric Guitar (clean)",
			"Electric Guitar (muted)",
			"Overdriven Guitar",
			"Distortion Guitar",
			"Guitar Harmonics",
			"Acoustic Bass",
			"Electric Bass (finger)",
			"Electric Bass (pick)",
			"Fretless Bass",
			"Slap Bass 1",
			"Slap Bass 2",
			"Synth Bass 1",
			"Synth Bass 2",
			"Violin",
			"Viola",
			"Cello",
			"Contrabass",
			"Tremolo Strings",
			"Pizzicato Strings",
			"Orchestral Strings",
			"Timpani",
			"String Ensemble 1 (strings)",
			"String Ensemble 2 (slow strings)",
			"SynthStrings 1",
			"SynthStrings 2",
			"Choir Aahs",
			"Voice Oohs",
			"Synth Voice",
			"Orchestra Hit",
			"Trumpet",
			"Trombone",
			"Tuba",
			"Muted Trumpet",
			"French Horn",
			"Brass Section",
			"SynthBrass 1",
			"SynthBrass 2",
			"Soprano Sax",
			"Alto Sax",
			"Tenor Sax",
			"Baritone Sax",
			"Oboe",
			"English Horn",
			"Bassoon",
			"Clarinet",
			"Piccolo",
			"Flute",
			"Recorder",
			"Pan Flute",
			"Blown Bottle",
			"Skakuhachi",
			"Whistle",
			"Ocarina",
			"Lead 1 (square)",
			"Lead 2 (sawtooth)",
			"Lead 3 (calliope)",
			"Lead 4 (chiff)",
			"Lead 5 (charang)",
			"Lead 6 (voice)",
			"Lead 7 (fifths)",
			"Lead 8 (bass+lead)",
			"Pad 1 (new age)",
			"Pad 2 (warm)",
			"Pad 3 (polysynth)",
			"Pad 4 (choir)",
			"Pad 5 (bowed)",
			"Pad 6 (metallic)",
			"Pad 7 (halo)",
			"Pad 8 (sweep)",
			"FX 1 (rain)",
			"FX 2 (soundtrack)",
			"FX 3 (crystal)",
			"FX 4 (atmosphere)",
			"FX 5 (brightness)",
			"FX 6 (goblins)",
			"FX 7 (echoes)",
			"FX 8 (sci-fi)",
			"Sitar",
			"Banjo",
			"Shamisen",
			"Koto",
			"Kalimba",
			"Bagpipe",
			"Fiddle",
			"Shanai",
			"Tinkle Bell",
			"Agogo",
			"Steel Drums",
			"Woodblock",
			"Taiko Drum",
			"Melodic Tom",
			"Synth Drum",
			"Reverse Cymbal",
			"Guitar Fret Noise",
			"Breath Noise",
			"Seashore",
			"Bird Tweet",
			"Telephone Ring",
			"Helicopter",
			"Applause",
			"Gunshot"};
			familyNum = 0;
			showInstrumentsFromeFamily();
			connect(this, SIGNAL(activated(int)), this, SLOT(myValueChanged(int)));
}

void InstrumentsBox::familyChanged(int family){
	familyNum = family;
	showInstrumentsFromeFamily();
	emit this->activated( 0 );//fst instrument in family
}
void InstrumentsBox::myValueChanged(int instrument){
	emit instrumentChanged(familyNum * 8 + instrument);
}
void InstrumentsBox::octaveChanged(int octave){

}
InstrumentsBox::~InstrumentsBox()
{

}
