#ifndef MIDIDEVICESINGLETON_H
#define MIDIDEVICESINGLETON_H

#include <QObject>
#include <QThread>

#include "..\midi\include\integers.h"
#include "..\midi\include\midi.h"
#include "..\midi\include\mididevice.h"
#include "..\midi\include\midievent.h"
#include "..\midi\include\midiheader.h"
#include "..\midi\include\miditrack.h"
#include "..\midi\include\rw_error.h"
#include <QObject>
#include <QThread>

#include <iostream>

class MidiDeviceSingleton : public QObject , public midi::MidiDevice
{
	Q_OBJECT
protected:
	MidiDeviceSingleton(QObject *parent) : QObject(parent){};
	MidiDeviceSingleton() : QObject(NULL){};
public:
	static MidiDeviceSingleton& Instance(){
		static MidiDeviceSingleton instance;
		return instance;
	}
private:
	~MidiDeviceSingleton(){};
	MidiDeviceSingleton(const MidiDeviceSingleton& root);
	MidiDeviceSingleton& operator=(const MidiDeviceSingleton&);
	public slots:
	/*void playEvent(midi::MidiEvent me){
		std::cout << "play " << std::endl;
		this->play(me,1);
	}*/
};



#endif // MIDIDEVICESINGLETON_H
