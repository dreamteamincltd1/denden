#ifndef KEYEVENTS_H
#define KEYEVENTS_H

#include <QWidget>
#include "ui_keyevents.h"

class KeyEvents : public QWidget
{
	Q_OBJECT

public:
	KeyEvents(QWidget *parent = 0);
	~KeyEvents();

private:
	Ui::KeyEvents ui;
};

#endif // KEYEVENTS_H
