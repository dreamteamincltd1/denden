#include "../common/keyboard.h"
#include "../midi/include/rw_error.h"
#include "../midi/include/midi.h"
#include "../midi/include/mididevice.h"

int main(int argc, char* argv[]) {
	if (argc < 2) {
		fprintf(stderr, "Usage:\tMidiPlay.exe \"File_to_play.mid\" [hq]\n");
		exit(-1);
	}
	char* file_midi = argv[1];
	
	FILE* midi_file = fopen(file_midi, "rb");
	if (!midi_file) {
		fprintf(stderr, "Unable to open MIDI file %s\n", file_midi);
		exit(-2);
	}
	fseek(midi_file, 0, SEEK_END);
	size_t file_size = ftell(midi_file);
	rewind(midi_file);
	uint8_t* buf = new uint8_t[file_size];
	fread(buf, 1, file_size, midi_file);
	fclose(midi_file);

	midi::Midi midi_data;
	try {
		midi_data.read(buf);
	} catch (rw_error e) {
		uint8_t* where = e.where();
		fprintf(stderr, "MIDI file reading error: %s at pos %u\n"
			"> %02x %02x %02x %02x ..\n", e.what(), where - buf,
			(unsigned)where[0], (unsigned)where[1], (unsigned)where[2], (unsigned)where[3]
			);
		exit(-3);
	} catch (std::exception e) {
		fprintf(stderr, "MIDI file error: %s\n", e.what());
		exit(-3);
	}
	delete[] buf;

	midi::Midi midi_merged = midi_data.merge();

	bool hq = (argc >= 3) && (argv[2][0] != '0');

	KeyReader kr;

	midi::MidiDevice md(hq);
	md.ticks_per_quarter_note(midi_merged.ticks_per_quarter_note());

	midi::MidiTrack& mt = midi_merged.tracks[0];
	size_t es = mt.events.size();

	bool playing = false;
	bool refresh = true;
	int pos = 0, pos_prev = 0;
	size_t e = 0;
	while (true) {
		if (playing) {
			if (e >= es || mt.events[e].type() == midi::MidiEvent::META_END_TRACK) {
				playing = false;
				e = 0;
				refresh = true;
			} else {
				md.play(mt.events[e++], true);
				pos = e * 80 / es;
				if (pos != pos_prev) {
					pos_prev = pos;
					refresh = true;
				}
			}
		} else {
			md.stop();
		}

		if (refresh) {
			int i = 0;
			system("cls");
			printf("File: %s\n", argv[1]);
			puts(hq ? "High-quality timing mode" : "Standart timing mode");
			printf("Status: ");
			puts(playing ? "Playing..." : "Stopped");
			putchar('\n');
			while (i < pos) {
				putchar('#');
				++i;
			}
			if (i < 80) {
				putchar(playing ? '>' : 'X');
				++i;
			}
			while (i < 80) {
				putchar('.');
				++i;
			}
			putchar('\n');
			puts(
				"Play/Pause:  SPACE" "\n"
				"Stop:        BACKSPACE" "\n"
				"Quit:        ESC");
			refresh = false;
		}

		kr.read();

		if (kr.pressed(VK_ESCAPE)) break;
		if (kr.pressed(VK_SPACE)) {
			playing = !playing;
			refresh = true;
		} else if (kr.pressed(VK_BACK)) {
			playing = false;
			e = 0;
			pos = 0;
			refresh = true;
		}

		Sleep(10);
	}

	return 0;
}