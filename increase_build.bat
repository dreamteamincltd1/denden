@echo off
:: Build incrementing script
::
set VERSION_FILE=version.txt
if not [%1]==[] set VERSION_FILE=%1
set MAJOR_MINOR_VERSION=0.0.0.
set /a BUILD_NUMBER=0

:: Check if file exists
if exist %VERSION_FILE% (
	:: Read file
	for /f "tokens=1-4 delims=." %%A in (%VERSION_FILE%) do (
		:: Read version
		set MAJOR_MINOR_VERSION=%%A.%%B.%%C.
		:: Read build and increment value
		set /a BUILD_NUMBER=%%D
	)
)

:: Increment build number
set /a BUILD_NUMBER+=1

set FULLVERSION=%MAJOR_MINOR_VERSION%%BUILD_NUMBER%

:: Write to file
> %VERSION_FILE% echo %FULLVERSION%

:: Display version
echo %FULLVERSION%