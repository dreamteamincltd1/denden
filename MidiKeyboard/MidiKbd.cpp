﻿#include <Windows.h>
#include <cstdio>
#include "../common/keyboard.h"
#include "../midi/include/midi.h"
#include "../midi/include/midievent.h"
#include "../midi/include/mididevice.h"

#define SEND_NOTE_OFF(o, k) (notes[midi::note_by_octave_key((o), (k))] = time_release)
#define SEND_NOTE_ON(o, k) (md.play(note_on.data(0, midi::note_by_octave_key((o), (k)))), notes[midi::note_by_octave_key((o), (k))] = time_release)

#define TAB "\t"
#define ENDL "\n"

#define SCREEN_HEADER \
"MIDI keyboard" ENDL \
"Instrument: %s / %s" ENDL \
"Prev group: {" TAB TAB "Next group: }" ENDL \
"Prev instr: [" TAB TAB "Next instr: ]" ENDL \
"Fade in:  %3u" TAB TAB "Less: 9" TAB "More: 0" ENDL \
"Fade out: %3u" TAB TAB "Less: (" TAB "More: )" ENDL \
"Hold time: %5u ms" TAB "Less: O" TAB "More: P" ENDL \
ENDL


#define SCREEN_KEYS \
"Prev octave: LEFT       %3i | %-4i     Next octave: RIGHT" ENDL \
"_________________________________________________________" ENDL \
"|  [#] [#]  |  [#] [#] [#]  |  [#] [#]  |  [#] [#] [#]  |" ENDL \
"|  [#] [#]  |  [#] [#] [#]  |  [#] [#]  |  [#] [#] [#]  |" ENDL \
"|   2   3   |   5   6   7   |   g   h   |   k   l   ;   |" ENDL \
"|  [#] [#]  |  [#] [#] [#]  |  [#] [#]  |  [#] [#] [#]  |" ENDL \
"|   |   |   |   |   |   |   |   |   |   |   |   |   |   |" ENDL \
"| q | w | e | r | t | y | u | v | b | n | m | , | . | / |" ENDL \
"|___|___|___|___|___|___|___|___|___|___|___|___|___|___|" ENDL \
ENDL \
"Percussion: %s" ENDL \
"Hit: SPACE" TAB "Prev: Z" TAB "Next: X" ENDL \
ENDL \
"All notes off: BACKSPACE" ENDL


const char MAIN_SCREEN[] = SCREEN_HEADER SCREEN_KEYS;


int main() {
	KeyReader kr;
	
	midi::MidiDevice md;

	bool refresh = true;
	bool instr_changed = false;
//	size_t sleep_ms = 20;
	
	int octave = 4, octave_next = 5;
	uint8_t instr_type = 0;
	uint8_t instr_group = 0;
	size_t hold_time = 250;

	midi::MidiEvent instrument = midi::MidiEvent::Instrument(0, 0);
	midi::MidiEvent note_off = midi::MidiEvent::NoteOff(0, 0);
	midi::MidiEvent note_on = midi::MidiEvent::NoteOn(0, 0);

	midi::MidiEvent perc_on = midi::MidiEvent::NoteOn(9, 35);
	midi::MidiEvent perc_off = midi::MidiEvent::NoteOff(9, 35);

	uint64_t *notes = new uint64_t[128];
	uint64_t *percs = new uint64_t[64];
	ZeroMemory(notes, sizeof(uint64_t) * 128);
	ZeroMemory(percs, sizeof(uint64_t) * 64);

	LARGE_INTEGER cpu_frq, cpu_tacts;
	QueryPerformanceFrequency(&cpu_frq);
	uint64_t wait_release = cpu_frq.QuadPart * hold_time / 1000;
	uint64_t time_release = 0;

	while (true) {
		// Wait
		Sleep(5);
		QueryPerformanceCounter(&cpu_tacts);
		time_release = cpu_tacts.QuadPart + wait_release;

		// Release notes
		for (size_t i = 0; i < 128; ++i) {
			if (notes[i] && (notes[i] <= cpu_tacts.QuadPart)) {
				md.play(note_off.data(0, i));
				notes[i] = 0;
			}
		}
		for (size_t i = 0; i < 64; ++i) {
			if (percs[i] && (percs[i] <= cpu_tacts.QuadPart)) {
				md.play(perc_off.data(0, i + 35));
				percs[i] = 0;
			}
		}
		
		// Check changes
		if (instr_changed) {
			uint8_t instr = (instr_group << 3) | (instr_type & 0x07);
			instrument.data(0, instr);
			md.play(instrument);
			instr_changed = false;
			refresh = true;
		}
		if (refresh) {
			system("cls");
			printf(MAIN_SCREEN, 
				instrument.instrument_family(), 
				instrument.instrument_name(),
				(unsigned)note_on.data(1),
				(unsigned)note_off.data(1),
				hold_time,
				octave,
				octave + 1,
				perc_on.note_name());
			refresh = false;
			Sleep(50);
		}
		
		kr.read();
		if (kr.pressed(VK_ESCAPE)) break;

		if (kr.pressed(VK_OEM_4)) {
			if (kr.down(VK_SHIFT)) {
				instr_group = (instr_group - 1) & 0x0f;
				instr_changed = true;
				continue;
			}
			instr_type = (instr_type - 1) & 0x07;
			instr_changed = true;
			continue;
		}
		if (kr.pressed(VK_OEM_6)) {
			if (kr.down(VK_SHIFT)) {
				instr_group = (instr_group + 1) & 0x0f;
				instr_changed = true;
				continue;
			}
			instr_type = (instr_type + 1) & 0x07;
			instr_changed = true;
			continue;
		}

		if (kr.pressed(VK_LEFT)) {
			if (octave > -1) {
				--octave;
				--octave_next;
			}
			refresh = true;
			continue;
		}
		if (kr.pressed(VK_RIGHT)) {
			if (octave_next < 12) {
				++octave;
				++octave_next;
			}
			refresh = true;
			continue;
		}
		if (kr.pressed('9')) {
			if (kr.down(VK_SHIFT)) {
				uint8_t fade = note_off.data(1);
				fade = (fade < 8) ? 0 : (fade >= 127) ? 120 : (fade - 8);
				note_off.data(1, fade);
			} else {
				uint8_t fade = note_on.data(1);
				fade = (fade < 8) ? 0 : (fade >= 127) ? 120 : (fade - 8);
				note_on.data(1, fade);
			}
			refresh = true;
			continue;
		}
		if (kr.pressed('0')) {
			if (kr.down(VK_SHIFT)) {
				uint8_t fade = note_off.data(1);
				fade = (fade >= 120) ? 127 : (fade + 8);
				note_off.data(1, fade);
			} else {
				uint8_t fade = note_on.data(1);
				fade = (fade >= 120) ? 127 : (fade + 8);
				note_on.data(1, fade);
			}
			refresh = true;
			continue;
		}
		if (kr.pressed('O')) {
			if (hold_time > 25) {
				hold_time -= 25;
				wait_release = cpu_frq.QuadPart * hold_time / 1000;
				refresh = true;
				continue;
			}
		}
		if (kr.pressed('P')) {
			if (hold_time < 2000) {
				hold_time += 25;
				wait_release = cpu_frq.QuadPart * hold_time / 1000;
				refresh = true;
				continue;
			}
		}

		if (kr.pressed('Z')) {
			uint8_t perc = perc_on.data(0);
			if (perc <= 35) perc = 81;
			else --perc;
			perc_on.data(0, perc);
			refresh = true;
			continue;
		}
		if (kr.pressed('X')) {
			uint8_t perc = perc_on.data(0);
			if (perc >= 81) perc = 35;
			else ++perc;
			perc_on.data(0, perc);
			refresh = true;
			continue;
		}


		if (kr.pressed(VK_BACK)) {
			md.play(midi::MidiEvent::Controller(0, 123, 0));
			md.play(midi::MidiEvent::Controller(9, 123, 0));
			ZeroMemory(notes, sizeof(uint64_t) * 128);
			ZeroMemory(percs, sizeof(uint64_t) * 64);
			continue;
		}

		if (kr.turned_off(VK_SPACE)) (percs[perc_on.data(0) - 35] = time_release);
		if (kr.pressed(VK_SPACE)) md.play(perc_on);


		// Process keys up
		if (kr.turned_off('Q')) SEND_NOTE_OFF(octave, 0);
		if (kr.turned_off('2')) SEND_NOTE_OFF(octave, 1);
		if (kr.turned_off('W')) SEND_NOTE_OFF(octave, 2);
		if (kr.turned_off('3')) SEND_NOTE_OFF(octave, 3);
		if (kr.turned_off('E')) SEND_NOTE_OFF(octave, 4);
		if (kr.turned_off('R')) SEND_NOTE_OFF(octave, 5);
		if (kr.turned_off('5')) SEND_NOTE_OFF(octave, 6);
		if (kr.turned_off('T')) SEND_NOTE_OFF(octave, 7);
		if (kr.turned_off('6')) SEND_NOTE_OFF(octave, 8);
		if (kr.turned_off('Y')) SEND_NOTE_OFF(octave, 9);
		if (kr.turned_off('7')) SEND_NOTE_OFF(octave, 10);
		if (kr.turned_off('U')) SEND_NOTE_OFF(octave, 11);
		if (kr.turned_off('V')) SEND_NOTE_OFF(octave_next, 0);
		if (kr.turned_off('G')) SEND_NOTE_OFF(octave_next, 1);
		if (kr.turned_off('B')) SEND_NOTE_OFF(octave_next, 2);
		if (kr.turned_off('H')) SEND_NOTE_OFF(octave_next, 3);
		if (kr.turned_off('N')) SEND_NOTE_OFF(octave_next, 4);
		if (kr.turned_off('M')) SEND_NOTE_OFF(octave_next, 5);
		if (kr.turned_off('K')) SEND_NOTE_OFF(octave_next, 6);
		if (kr.turned_off(VK_OEM_COMMA)) SEND_NOTE_OFF(octave_next, 7);
		if (kr.turned_off('L')) SEND_NOTE_OFF(octave_next, 8);
		if (kr.turned_off(VK_OEM_PERIOD)) SEND_NOTE_OFF(octave_next, 9);
		if (kr.turned_off(VK_OEM_1)) SEND_NOTE_OFF(octave_next, 10);
		if (kr.turned_off(VK_OEM_2)) SEND_NOTE_OFF(octave_next, 11);

		// Process keys down
		if (kr.pressed('Q')) SEND_NOTE_ON(octave, 0);
		if (kr.pressed('2')) SEND_NOTE_ON(octave, 1);
		if (kr.pressed('W')) SEND_NOTE_ON(octave, 2);
		if (kr.pressed('3')) SEND_NOTE_ON(octave, 3);
		if (kr.pressed('E')) SEND_NOTE_ON(octave, 4);
		if (kr.pressed('R')) SEND_NOTE_ON(octave, 5);
		if (kr.pressed('5')) SEND_NOTE_ON(octave, 6);
		if (kr.pressed('T')) SEND_NOTE_ON(octave, 7);
		if (kr.pressed('6')) SEND_NOTE_ON(octave, 8);
		if (kr.pressed('Y')) SEND_NOTE_ON(octave, 9);
		if (kr.pressed('7')) SEND_NOTE_ON(octave, 10);
		if (kr.pressed('U')) SEND_NOTE_ON(octave, 11);
		if (kr.pressed('V')) SEND_NOTE_ON(octave_next, 0);
		if (kr.pressed('G')) SEND_NOTE_ON(octave_next, 1);
		if (kr.pressed('B')) SEND_NOTE_ON(octave_next, 2);
		if (kr.pressed('H')) SEND_NOTE_ON(octave_next, 3);
		if (kr.pressed('N')) SEND_NOTE_ON(octave_next, 4);
		if (kr.pressed('M')) SEND_NOTE_ON(octave_next, 5);
		if (kr.pressed('K')) SEND_NOTE_ON(octave_next, 6);
		if (kr.pressed(VK_OEM_COMMA)) SEND_NOTE_ON(octave_next, 7);
		if (kr.pressed('L')) SEND_NOTE_ON(octave_next, 8);
		if (kr.pressed(VK_OEM_PERIOD)) SEND_NOTE_ON(octave_next, 9);
		if (kr.pressed(VK_OEM_1)) SEND_NOTE_ON(octave_next, 10);
		if (kr.pressed(VK_OEM_2)) SEND_NOTE_ON(octave_next, 11);
		/**/
	} /**/

	delete[] notes;
	delete[] percs;
	return 0;
}