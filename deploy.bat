@echo off
:: Deploying new program version script
set PROGRAM=DenDen
set EXENAME=MidiVisual.exe

:: Add custom paths
set PATH=%QTDIR%\bin\;C:\Program Files\7-Zip\;%PATH%
set VCINSTALLDIR=%VS120COMNTOOLS%\..\..\VC\

echo Downloading latest revision...
git pull origin master

echo Building solution vith MSVC...
"%VS120COMNTOOLS%\..\IDE\devenv.exe" %PROGRAM%.sln /Rebuild Release

echo Adding version number...
for /f "delims=" %%v in ('increase_build build\version.txt') do @set VERSION=%%v
echo New version: %VERSION%

echo Deploying QT libs...
windeployqt --release --no-translations --no-webkit2 build\%EXENAME%

echo Making installer...
set SETUP_OUT=..\setup
set SETUP_FILE=%PROGRAM%_%VERSION%_setup.exe
cd build
del /q *.pdb
rmdir /s /q imageformats
del /q %SETUP_OUT%\*.*
mkdir %SETUP_OUT%\%PROGRAM%
xcopy /S /Y * %SETUP_OUT%\%PROGRAM%
cd %SETUP_OUT%
7z a -sfx7z.sfx %SETUP_FILE% %PROGRAM%\*
rmdir /s /q %PROGRAM%
cd ..

echo Commiting changes...
git add setup/*_setup.exe -f
git commit -a -m "New program version released: %VERSION%"

echo Uploading to server...
git push origin master

pause