#pragma once
#include <Windows.h>

class KeyReader {
public:
	KeyReader() {
		ZeroMemory(_down, 256 * sizeof(size_t));
		_lifetime = 1;
	}

	bool read(size_t lifetime = 1) {
		bool pressed = false;
		_lifetime = lifetime;
		for (size_t i = 0; i < 256; ++i) {
			_prev[i] = _down[i];
			if (GetAsyncKeyState(i) & 0xfff0) {
				_down[i] = lifetime;
				pressed = true;
			} else if (_down[i]) {
				--_down[i];
			}
		}
		return pressed;
	}

	// Key is in active state
	bool on(int vkey) const { return _down[vkey]; }

	// Key is in passive state
	bool off(int vkey) const { return !_down[vkey]; }

	// Key is down
	bool down(int vkey) const { return _down[vkey] == _lifetime; }

	// Key is holding
	bool holding(int vkey) const { return (_down[vkey] == _lifetime) && (_prev[vkey] == _lifetime); }

	// Key was pressed
	bool pressed(int vkey) const { return _down[vkey] > _prev[vkey]; }

	// Key was released
	bool released(int vkey) const { return _down[vkey] < _prev[vkey]; }

	// Key was pressed first time
	bool pressed_first(int vkey) const { return _down[vkey] && !_prev[vkey]; }

	// Keys enters passive state
	bool turned_off(int vkey) const { return !_down[vkey] && _prev[vkey]; }

private:
	size_t _down[256];
	size_t _prev[256];
	size_t _lifetime;
};